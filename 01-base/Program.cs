﻿using System;
using System.Text;

namespace _01_base
{
    // Une enumération est un ensemble de constante
    enum Motorisation { ESSENCE = 95, DIESEL = 3, ELECTRIQUE = 2, GPL = 4 } // par défaut int

    // Par défaut, une énumération est de type int , on peut définir un autre type qui ne peut-être que de type intégral
    enum Direction : short { NORD, SUD, EST, OUEST }

    // Énumération comme indicateurs binaires
    [Flags]
    enum JourSemaine
    {
        LUNDI = 1,
        MARDI = 2,
        MERCREDI = 4,
        JEUDI = 8,
        VENDREDI = 16,
        SAMEDI = 32,
        DIMANCHE = 64,
        WEEKEND = SAMEDI | DIMANCHE
    }

    // Définition d'une structure
    struct Point
    {
        public int x;
        public int y;
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            #region Déclaration de variable
            // Déclaration d'une variable   type nomVariable;
            int i;
            //Console.WriteLine(i); // Erreur => on ne peut pas utiliser une variable locale non initialisée

            // Initialisation    nomVariable = valeur
            i = 42;
            Console.WriteLine(i);

            // Déclaration et initialisation de variable 
            int j = 1;

            // Déclaration multiple de variable
            double largeur = 123.0, hauteur = 456.0;
            Console.WriteLine(j + " " + largeur + " " + hauteur); // + => concaténation
            #endregion

            #region Littéral
            // Littéral booléen
            bool tst = false; // ou true
            Console.WriteLine(tst);

            // Littéral caractère
            char chr = 'a';
            char chrUtf8 = '\u0045';    // Caractère en UTF-8
            char chrUtf8Hexa = '\x45';
            Console.WriteLine(chr + " " + " " + chrUtf8 + " " + chrUtf8Hexa);

            // Littéral entier -> int par défaut
            long l = 123L; // L > long
            uint ui = 123U; // U -> unsigned
            Console.WriteLine(l + " " + ui);

            // Littéral entier -> changement de base
            int dec = 123;          // décimal (base 10) par défaut
            int hexa = 0xFF2;       // 0x => héxadécimal (base 16)
            int bin = 0b010101;     // 0b => binaire (base 2)
            Console.WriteLine(dec + " " + hexa + " " + bin);

            // Littéral nombre à virgule flottante
            double d1 = 12.3;
            double d2 = .5;
            double d3 = 1.23e3;
            Console.WriteLine(d1 + " " + d2 + " " + d3);

            // Littéral nombre à virgule flottante -> par défaut double
            float f = 12.3F;        // F -> float
            decimal deci = 12.3M;   // M -> Decimal
            Console.WriteLine(f + " " + deci);
            #endregion

            #region Typage implicite
            // Type implicite -> var
            // Le type est déterminé par le type de la littérale, de l'expression ou du retour d'une méthode
            var v1 = 12.3F;         // v1 -> float
            var v2 = "Hello world"; // v2 -> string
            var v3 = i + j;         // v3-> int le résultat de i + j est un entier
            Console.WriteLine(v1 + " " + v2 + " " + v3);
            #endregion

            // avec @ on peut utiliser les mots réservés comme nom de variable (uniquement si nécessaire)
            int @while = 1;
            Console.WriteLine(@while);

            #region Transtypage 
            // Transtypage autormatique (pas de perte de donnée)
            // type inférieur => type supérieur
            int ti1 = 45;
            double ti2 = ti1;
            long ti3 = ti1;
            Console.WriteLine(ti1 + " " + ti2 + " " + ti3);

            // Transtypage explicite: cast -> (nouveauType)
            double te1 = 12.3;
            float te2 = (float)te1;
            int te3 = (int)te1;
            Console.WriteLine(te1 + " " + te2 + " " + te3);
            #endregion
            #region Dépassement de capacité
            int dep1 = 300;             // 00000000 00000000 00000001 00101100    300
            sbyte dep2 = (sbyte)dep1;   //                            00101100    44
            Console.WriteLine(dep2);

            // checked => checked permet d’activer explicitement le contrôle de dépassement de capacité 
            // pour les conversions et les opérations arithmétiques de type intégral
            //checked
            //{
            //    dep1 = 300;
            //    dep2 = (sbyte)dep1;
            //    Console.WriteLine(dep2);    // avec checked une exception est générée, s'il y a un dépassement de capacité
            //}

            // Par défaut la vérification des dépassement de capacités est unchecked
            // si l'on veut qu'elles soient par défaut checked: il faut choisir l'option checked dans les propriétés du projet dans Advanced Build Settings
            // Dans ce cas, on utilise unchecked pour désactiver la vérification des dépassements
            unchecked
            {
                dep1 = 300;
                dep2 = (sbyte)dep1;
                Console.WriteLine(dep2);     // plus de vérification de dépassement de capacité
            }
            #endregion

            #region Fonction de conversion
            // La classe Convert contient des méthodes statiques permettant de convertir un entier vers un double, ...
            // On peut aussi convertir une chaine de caractères en un type numérique
            double fc1 = 12.3;
            float fc2 = Convert.ToSingle(fc1);
            Console.WriteLine(fc2);

            int fc3 = Convert.ToInt32("42");
            Console.WriteLine(fc3);

            // int fc = Convert.ToInt32("azerty"); //  Erreur => génère une exception formatException

            // Conversion d'un nombre sous forme de chaine dans une autre base
            Console.WriteLine(Convert.ToString(fc3, 2)); // binaire
            Console.WriteLine(Convert.ToString(fc3, 16)); // hexa

            // Conversion d'une chaine de caractères en entier
            // Parse
            int fc4 = int.Parse("123");
            //fc4 = int.Parse("azery");     //  Erreur => génère une exception formatException
            Console.WriteLine(fc4);

            // TryParse
            int fc5;
            bool tstCnv = int.TryParse("123", out fc5); // Retourne true et la convertion est affecté à fc5
            Console.WriteLine(tstCnv + " " + fc5);

            tstCnv = int.TryParse("123", out fc5); // Retourne false, 0 est affecté à fc5
            Console.WriteLine(tstCnv + " " + fc5);
            #endregion

            #region Type référence
            StringBuilder s1 = new StringBuilder("azerty");
            StringBuilder s2 = null;
            Console.WriteLine(s1 + " " + s2);
            s2 = s1;
            Console.WriteLine(s1 + " " + s2);
            s1 = null;
            Console.WriteLine(s1 + " " + s2);
            s2 = null;  // s1 et s2 sont égales à null
                        // Il n'y a plus de référence sur l'objet, il est éligible à la destruction par le garbage collector
            Console.WriteLine(s1 + " " + s2);
            #endregion

            #region Type nullable
            // Nullable : permet d'utiliser une valeur null avec un type valeur
            bool? tn1 = null;
            Console.WriteLine(tn1.HasValue + " " + (tn1 != null));  // tn1 == null retourne false
            tn1 = false;                                            // Conversion implicite(bool vers nullable)
            Console.WriteLine(tn1.HasValue + " " + (tn1 != null));  // La propriété HasValue retourne true si tn1 contient une valeur (!= null)

            Console.WriteLine(tn1.Value);   // Pour récupérer la valeur, on peut utiliser la propriété Value
            bool tst2 = (bool)tn1;          // ou, on peut faire un cast
            Console.WriteLine(tst2);
            #endregion

            #region Constante
            const double PI = 3.14;
            // PI=3.1419;    // Erreur: on ne peut pas modifier la valeur d'une constante
            Console.WriteLine(PI * 2);
            // const int TI;   // Erreur: on est obligé d'initialiser une constante
            #endregion

            #region Format de chaine de caractère
            int xi = 1;
            int yi = 5;
            string resFormat = string.Format("xi={0} yi={1}", xi, yi);
            Console.WriteLine(resFormat);
            Console.WriteLine("xi={0} yi={1}", xi, yi); // On peut définir directement le format dans la mèthode WriteLine

            Console.WriteLine($"xi={xi} yi={yi} xi x yi={xi * yi}");

            Console.WriteLine("c:\tmp\newfile.txt");     // \t et \n sont considérés comme des caractères spéciaux

            // Caractères spéciaux
            // \n       Nouvelle ligne
            // \r       Retour chariot
            // \f       Saut de page
            // \t       Tabulation horizontale
            // \v       Tabulation verticale
            // \0       Caractère nul
            // \a 	    Alerte
            // \b       Retour arrière
            // \\       Backslash
            // \'       Apostrophe
            // \"       Guillemet

            // Chaînes textuelles => @ devant une littérale chaine de caractères
            // Conserve les retours à la ligne, n'interpréte pas les caractères spéciaux (utile pour les chemins de fichiers)
            Console.WriteLine(@"c:\tmp\newfile.txt");
            #endregion

            #region Opérateur
            // opérateur arithmétique
            // division par 0
            // Entier
            //int zero = 0;
            //Console.WriteLine(1 / zero);

            // Nombre virgule flottante
            Console.WriteLine(1.0 / 0.0);   // double.PositiveInfinity
            Console.WriteLine(-1.0 / 0.0);  // double.NegativeInfinity
            Console.WriteLine(0.0 / 0.0);   // double.NaN

            // Incrémentation
            // pré-incrémentation
            int inc = 0;
            int res = ++inc;
            Console.WriteLine($"inc={inc} res={res}"); //inc ==1 res==1

            // post-incrémentation
            inc = 0;
            res = inc++;
            Console.WriteLine($"inc={inc} res={res}"); //inc ==1 res==0

            // Affectation composée
            res = 12;
            res += 23; // res=res+23
            Console.WriteLine(res);

            // Opérateur de comparaison
            bool tst3 = res > 100;      // Une comparaison a pour résultat un booléen
            Console.WriteLine(tst3);    // false

            // Opérateur logique
            // Opérateur Non !
            Console.WriteLine(!tst3);   // true

            // Opérateur court-circuit && et ||
            // && -> dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
            bool tst4 = res > 100 && res < 10; // false
            Console.WriteLine(tst4);

            // || -> dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
            bool tst5 = res > 0 || res < 10; // true
            Console.WriteLine(tst5);

            // Opérateur Binaires (bit à bit)
            byte b = 0b11010;
            Console.WriteLine(Convert.ToString(~b, 2));         // Opérateur ~ => complémént: 1-> 0 et 0-> 1
            Console.WriteLine(Convert.ToString(b & 0b1101, 2)); //          et => 1000
            Console.WriteLine(Convert.ToString(b | 0b100, 2));  //          ou =>  11110
            Console.WriteLine(Convert.ToString(b ^ 0b10, 2));   // ou exclusif =>  11000

            Console.WriteLine(Convert.ToString(b >> 2, 2));       // Décalage à droite de 2 (insertion de 2 0 à gauche) => 110
            Console.WriteLine(Convert.ToString(b << 1, 2));     // Décalage à gauche de 1 (insertion de 1 0 à droite) => 110100

            // L’opérateur de fusion null ?? retourne la valeur de l’opérande de gauche si elle n’est pas null
            // sinon, il évalue l’opérande de droite et retourne son résultat
            string str5 = "azerty";
            string resStr = str5 ?? "valeur default";
            Console.WriteLine(resStr);   // azerty
            str5 = null;
            resStr = str5 ?? "valeur default";  // valeur défaut
            Console.WriteLine(resStr);
            #endregion

            #region Promotion numérique
            // pn1 est promu en double => Le type le + petit est promu vers le +grand type des deux
            int pn1 = 11;
            double pn2 = 12.3;
            double res2 = pn1 + pn2;
            Console.WriteLine(res2);

            Console.WriteLine(pn1 / 2); // 5
            Console.WriteLine(pn1 / 2.0); // 5.5  pn1 est promu en double

            // sbyte, byte, short, ushort, char sont promus en int
            short sh1 = 1;
            short sh2 = 5;          // sbyte, byte, short, ushort, char sont promus en int
            int res3 = sh1 + sh2;     // sh1 et sh2 sont promus en int
            Console.WriteLine(res3);
            #endregion

            #region Exercice Somme
            // Saisir 2 chiffres et afficher le résultat dans la console sous la forme 1 __ + __ 3 __ = __ 4
            Console.WriteLine("Somme: saisir 2 entiers");
            int val1 = Convert.ToInt32(Console.ReadLine());
            int val2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"{val1} + {val2} = {val1 + val2}");
            #endregion

            #region Exercice Moyenne: 
            // Saisir 2 nombres entiers et afficher la moyenne dans la console
            Console.WriteLine("Moyenne: saisir 2 entiers");
            int va1 = Convert.ToInt32(Console.ReadLine());
            int va2 = Convert.ToInt32(Console.ReadLine());
            double moyenne = (va1 + va2) / 2.0;
            Console.WriteLine($"moyenne={moyenne}");
            #endregion

            #region Enumération
            // m est une variable qui ne pourra accepter que les valeurs de l'enum Motorisation
            Motorisation m = Motorisation.GPL;

            //  enum -> string
            // La méthode toString convertit une constante enummérée en une chaine de caractère
            string strM = m.ToString();
            Console.WriteLine(strM);    // GPL

            // enum ->  entier (cast)
            int iM = (int)m;
            Console.WriteLine(iM);

            // string -> enum
            // La méthode Parse permet de convertir une chaine de caractère en une constante enummérée
            Motorisation mr = (Motorisation)Enum.Parse(typeof(Motorisation), "ESSENCE");
            Console.WriteLine(mr.ToString()); // mr=Motorisation.ESSENCE

            // m = (Motorisation)Enum.Parse(typeof(Motorisation), "GPL2"); // Si la chaine n'existe pas dans l'énumation => exception
            // Console.WriteLine(m);

            // int -> enum
            iM = 4;
            if (Enum.IsDefined(typeof(Motorisation), iM)) // Permet de tester si la valeur entière existe dans l'enumération
            {
                mr = (Motorisation)iM;
                Console.WriteLine(mr.ToString());
            }

            // Énumération comme indicateurs binaires
            JourSemaine js = JourSemaine.LUNDI | JourSemaine.MERCREDI;

            // Avec l'attribut [Flag] -> affiche LUNDI, MERCREDI , sans -> affiche 5
            Console.WriteLine(js.ToString());

            if ((js & JourSemaine.LUNDI) != 0)   // teste la présence de LUNDI
            {
                Console.WriteLine("Lundi");
            }
            if ((js & JourSemaine.MARDI) != 0)   // teste la présence de MARDI
            {
                Console.WriteLine("Mardi");
            }
            js |= JourSemaine.SAMEDI;   // jourSemaine = jourSemaine | Jours.SAMEDI;

            if ((js & JourSemaine.WEEKEND) != 0)
            {
                Console.WriteLine("Week-End");
            }

            // Utilisation d'une énumération avew switch
            switch (mr)
            {
                case Motorisation.ESSENCE:
                    Console.WriteLine("Essence");
                    break;
                case Motorisation.DIESEL:
                    Console.WriteLine("Diesel");
                    break;
                default:
                    Console.WriteLine("Autre motorisation");
                    break;
            }
            #endregion
            #region Structure
            Point p1;
            p1.x = 3;   // accès au champs x de la structure
            p1.y = 1;
            Console.WriteLine($"P1 x={p1.x} y={p1.y}");

            // Affectation d'une structure
            Point p2 = p1;
            Console.WriteLine($"P2 x={p2.x} y={p2.y}"); // Les champs de p2 sont initialisé avec les valeurs des champs de p1

            // Comparaison de 2 structures => Il faut comparer chaque champs
            if (p1.x == p2.x && p1.y == p2.y)
            {
                Console.WriteLine("Les points sont égaux");
            }
            #endregion
            Console.ReadKey();
        }
    }
}
