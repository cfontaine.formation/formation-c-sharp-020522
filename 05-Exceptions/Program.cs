﻿using System;

namespace _05_Exceptions
{
    internal class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int[] t = new int[5];
                Console.WriteLine("Entrer un indice du tableau");
                int index = Convert.ToInt32(Console.ReadLine());  // Peut générer une exception FormatException, si on saisie autre chose qu'un nombre entier
                Console.WriteLine(t[index]);                      // Peut générer une exception IndexOutOfRangeException , si index est > à Length
                int age = Convert.ToInt32(Console.ReadLine());    // Peut générer une exception FormatException, si on saisie autre chose qu'un nombre entier
                TraitementEmploye(age);
                Console.WriteLine("La suite du programme");
            }
            catch (IndexOutOfRangeException)  // Attrape les exceptions IndexOutOfRangeException
            {
                Console.WriteLine("L'index est en dehors des limites du tableau");  // Message => permet de récupérer le messsage de l'exception
            }
            catch (FormatException e)  // Attrape les exceptions FormatException
            {
                Console.WriteLine("Format de sasie incorrecte");
                Console.WriteLine(e.Message);
            }
            catch (Exception e)   // Attrape tous les autres exception
            {
                Console.WriteLine("Autre exception");
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
            finally    // Le bloc finally est toujours éxécuté
            {
                // finally -> généralement utilisé pour libérer les ressources
                Console.WriteLine("Finally -> toujours exécuté");
            }
            Console.WriteLine("Fin programme");

            // Exercice Saisir Nombre
            Console.WriteLine(SaisirNombre());

            // Exercice Calculatrice
            try
            {
                double a = Convert.ToDouble(Console.ReadLine());
                string op = Console.ReadLine();
                double b = Convert.ToDouble(Console.ReadLine());
                Calculatrice(a, b, op);
            }
            catch (FormatException)
            {
                Console.WriteLine("Erreur dans la saisie des nombres");
            }
            catch (ArithmeticException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.ReadLine();
        }

        static void TraitementEmploye(int age)
        {
            Console.WriteLine("Début du traitement de l'employé");
            try
            {
                TraitementAge(age);
            }
            catch (AgeNegatifException e) when (e.Age < -10)
            {
                // Traitement partiel de l'exception AgeNegatifException
                Console.WriteLine($"Traitement local partiel:{e.Message}");
                throw new Exception("Traitement Employé age négatif", e);    // On relance une exception différente
                                                                             // e => référence à l'exception qui a provoquer l'exception
            }
            catch (AgeNegatifException e)
            {
                // Traitement partiel de l'exception AgeNegatifException
                Console.WriteLine($"Traitement local partiel:{e.Message}");
                throw; // On relance l'exception pour que l'utilisateur de la méthode la traite a son niveau           
            }
            Console.WriteLine("Fin du traitement de l'employé");
        }

        static void TraitementAge(int age)
        {
            Console.WriteLine("Début du traitemnt de l'age");
            if (age < 0)
            {
                throw new AgeNegatifException(age, $"Age négatif({age})");  //  throw => Lancer un exception
                                                                            //  Elle va remonter la pile d'appel des méthodes jusqu'à ce qu'elle soit traité par un try/catch
                                                                            //  si elle n'est pas traiter, aprés la méthode Main => arrét du programme
            }
            Console.WriteLine("Fin du traitemnt de l'age");
        }

        #region Saisir un nombre
        // On crée une méthode double saisirNombre() qui permet de saisir un double au clavier  
        // - La méthode va gérer l'exception FormatException en affichant un message d'erreur  
        // - la saisie clavier recommence tant que l'on n'a pas saisie un nombre correct
        static double SaisirNombre()
        {
            while (true)
            {
                try
                {
                    int val = Convert.ToInt32(Console.ReadLine());
                    return val;
                }
                catch (FormatException e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }
        #endregion

        #region Exercice Calculatrice
        // On reprend l'exercice calculatrice, on place le code de la calculatrice dans une méthode qui prend en paramètre les 2 valeurs double et l'opérateur une chaine de caractère
        // à la place des messages d'erreur, on va générer des exceptions et le message est placé dans l'exception:
        // - pour la division par 0: une exception ArithmeticException
        // - pour l'opérateur non supporté : une exception Exception

        // Dans le Main: on appelle la méthode de la Calculatrice est on gére les 2 exceptions en affichant le message associé à l'exception
        static void Calculatrice(double v1, double v2, string op)
        {
            switch (op)
            {
                case "+":
                    Console.WriteLine($"{v1} + {v2} = {v1 + v2}");
                    break;
                case "-":
                    Console.WriteLine($"{v1} - {v2} = {v1 - v2}");
                    break;
                case "*":
                    Console.WriteLine($" {v1} * {v2} = {v1 * v2}");
                    break;
                case "/":
                    if (v2 == 0.0)
                    {
                        throw new ArithmeticException("Division par 0");
                    }
                    else
                    {
                        Console.WriteLine($" {v1} / {v2} = { v1 / v2}");
                    }
                    break;
                default:
                    throw new Exception($"L'opérateur {op} est incorrecte");
            }

        }
        #endregion
    }
}
