﻿using _15_AdoDotNet;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;

namespace _15_Bdd
{
    internal class Program
    {
        // Pour ajouter au projet la bibliothèque
        // Cliquer droit sur le projet -> Ajouter -> référence -> choisir 15-Bibliotheque-AdoDotNet
        static void Main(string[] args)
        {
            Contact contact = new Contact("John", "Doe", "jdoe@dawan.com", new DateTime(2000,4,1));
            // Test de connexion à la base de donnée (SQLServer)
            TestBdd(contact);
            // Test de Contact dao
            TestDao(contact);
            Console.ReadKey();
        }

        static void TestBdd(Contact contact)
        {
            // La chaine de connection contient toutes les informations pour la connection à la base de donnée
            // Server-> adresse du serveur de bdd
            // Port port de serveur de bdd
            // Database -> nom de la base de donnée
            // Uid -> utilisateur de la base de donnée
            // Pwd -> mot de passe de la bdd


            string chaineConnection = @"Data Source=DESKTOP-9H6VFME\SQLEXPRESS;Initial Catalog=formationmai;Integrated Security= True";

            // Création de la connexion à la base de donnée SqlConnection
            SqlConnection cnx = new SqlConnection(chaineConnection);
            // Ouverture de la connexion à la base de donnée
            cnx.Open();
            string requete = "INSERT INTO contacts(prenom,nom,email,date_naissance) VALUES (@prenom,@nom,@email,@dateNaissance);";
            SqlCommand cmd = new SqlCommand(requete, cnx);
            // Remplacement des paramètres dans la requête (@...) par les valeurs
            cmd.Parameters.AddWithValue("@prenom", contact.Prenom);
            cmd.Parameters.AddWithValue("@nom", contact.Nom);
            cmd.Parameters.AddWithValue("@email", contact.Email);
            cmd.Parameters.AddWithValue("@dateNaissance", contact.DateNaissance);
            cmd.ExecuteNonQuery();  // execution de la requete pour INSERT,UPDATE,DELETE

            requete = "SELECT prenom,nom,email,date_naissance FROM contacts;";
            cmd = new SqlCommand(requete, cnx);
            SqlDataReader r = cmd.ExecuteReader();
            while (r.Read())    // Read() permet de passer à la prochaine "ligne" retourne false quand il n'y a plus de resultat
            {
                Console.WriteLine($"{r.GetString(0)} {r.GetString(1)} {r.GetString(2)}  {r.GetDateTime(3).ToShortDateString()}");
            }
            // Fermeture de la connection
            cnx.Close();
            cnx.Dispose();
        }

        static void TestDao(Contact contact)
        {
            // Récupération de la chaine de connection dans le fichier App.config du projet élément<connectionStrings>
            ContactDao.ChaineConnection = ConfigurationManager.ConnectionStrings["chsqlserver"].ConnectionString;

            Contact c = SaisirContact();

            ContactDao dao = new ContactDao();
            Console.WriteLine("id={0}", c.Id); // id=0 => l'objet n'a pas été peristé dans la base de donnée
            dao.SaveOrUpdate(c); // persister l'objet dans la bdd
            Console.WriteLine("id={0}", c.Id); // id a été généré par la bdd
            Console.WriteLine("{0}", c);

            long id = c.Id;
            // Affichage de tous les objets contact
            List<Contact> lst = dao.FindAll();
            foreach (Contact co in lst)
            {
                Console.WriteLine(co);
            }

            // Lire un objet à partir de son id
            Console.WriteLine("\nLire {0}", id);
            Contact cr = dao.FindById(id);
            Console.WriteLine(cr);

            // Modification 
            Console.WriteLine("\nModification {0}", cr.Id);
            cr.Prenom = "Marcel";
            cr.DateNaissance = new DateTime(1987, 8, 11);
            dao.SaveOrUpdate(cr);
            cr = dao.FindById(cr.Id);
            Console.WriteLine(cr);

            // Effacer
            Console.WriteLine("\neffacer {0}", cr.Id);
            dao.Delete(cr);
            lst = dao.FindAll();
            foreach (Contact co in lst)
            {
                Console.WriteLine(co);
            }
        }

        private static Contact SaisirContact()
        {
            Console.Write("Entrer votre prénom: ");
            string prenom = Console.ReadLine();
            Console.Write("Entrer votre nom: ");
            string nom = Console.ReadLine();
            Console.Write("Entrer votre email: ");
            string email = Console.ReadLine();
            Console.Write("Entrer votre date de naissance (YYYY/MM/DD): ");
            DateTime jdn = DateTime.Parse(Console.ReadLine());
            return new Contact(prenom, nom, email,jdn);
        }

    }
}
