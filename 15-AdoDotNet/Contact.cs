﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _15_AdoDotNet
{
    public class Contact : AbstractEntity
    {
        public string Prenom { get; set; }

        public string Nom { get; set; }

        public string Email { get; set; }

        public DateTime DateNaissance { get; set; }

        public Contact(string prenom, string nom, string email, DateTime dateNaissance)
        {
            Prenom = prenom;
            Nom = nom;
            Email = email;
            DateNaissance = dateNaissance;
        }

        public override string ToString()
        {
            return $"{base.ToString()} {Prenom} {Nom} {Email} {DateNaissance.ToShortDateString()}";
        }
    }
}
