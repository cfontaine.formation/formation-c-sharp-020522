﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _15_AdoDotNet
{
    // La classe abstract AbstractEntity est la classe mère des objets persistés dans la base de données
    // Elle gère l'id qui fait le lien entre l'objet et la table des la bdd (clé primaire) 
    public abstract class AbstractEntity
    {
        public long Id { get; set; }

        // On compare les objets par rapport à leur id (2 objets avec un id égal sont égaux)
        public override bool Equals(object obj)
        {
            return obj is AbstractEntity entity &&
                   Id == entity.Id;
        }

        public override int GetHashCode()
        {
            return 2108858624 + Id.GetHashCode();
        }

        public override string ToString()
        {
            return $"{Id}";
        }


    }
}
