﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace _15_AdoDotNet
{
    public class ContactDao : GenericDao<Contact>
    {

        protected override void Create(SqlConnection cnx, Contact elm)
        {
            // SELECT SCOPE_IDENTITY() => permet de récupérer l'id généré par la base de donnée 
            string requete = "INSERT INTO contacts(prenom,nom,email,date_naissance) VALUES (@prenom,@nom,@email,@dateNaissance);SELECT SCOPE_IDENTITY();";
            using (SqlCommand cmd = new SqlCommand(requete, cnx))
            {
                cmd.Parameters.AddWithValue("@prenom", elm.Prenom);
                cmd.Parameters.AddWithValue("@nom", elm.Nom);
                cmd.Parameters.AddWithValue("@email", elm.Email);
                cmd.Parameters.AddWithValue("@dateNaissance", elm.DateNaissance);
                elm.Id = Convert.ToInt64(cmd.ExecuteScalar());    // ExecuteScalar pour executer la requête pour récupérer l'id généré par la bdd

            }
        }
        protected override void Delete(SqlConnection cnx, long id)
        {
            string req = "DELETE FROM contacts WHERE id=@id";
            using (SqlCommand cmd = new SqlCommand(req, cnx))
            {
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
            }
        }

        protected override List<Contact> FindAll(SqlConnection cnx)
        {
            List<Contact> contacts = new List<Contact>();
            string req = "SELECT id,prenom,nom,email,date_naissance FROM contacts";
            using (SqlCommand cmd = new SqlCommand(req, cnx))
            {
                using (SqlDataReader read = cmd.ExecuteReader())
                {
                    while (read.Read())
                    {
                        Contact contact = new Contact(read.GetString(1), read.GetString(2), read.GetString(3), read.GetDateTime(4));
                        contact.Id = read.GetInt64(0);
                        contacts.Add(contact);
                    }
                }
            }
            return contacts;
        }

        protected override Contact FindById(SqlConnection cnx, long id)
        {
            Contact contact = null;
            string req = "SELECT prenom,nom,email,date_naissance FROM contacts  WHERE id=@id";
            using (SqlCommand cmd = new SqlCommand(req, cnx))
            {
                cmd.Parameters.AddWithValue("@id", id);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        contact = new Contact(reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetDateTime(3));
                        contact.Id = id;
                    }
                }
            }
            return contact;

        }

        protected override void Update(SqlConnection cnx, Contact elm)
        {
            string req = "UPDATE contacts SET nom = @nom, prenom=@prenom, email=@email, date_naissance=@date_naissance WHERE id=@id";
            using (SqlCommand cmd = new SqlCommand(req, cnx))
            {
                cmd.Parameters.AddWithValue("@id", elm.Id);
                cmd.Parameters.AddWithValue("@prenom", elm.Prenom);
                cmd.Parameters.AddWithValue("@nom", elm.Nom);
                cmd.Parameters.AddWithValue("@date_naissance", elm.DateNaissance);
                cmd.Parameters.AddWithValue("@email", elm.Email);
                cmd.ExecuteNonQuery();

            }
        }

        // Dans le dao, on peut déclarer d'autre méthodes suplémentaires uniquement pour la classe Contact 
        public bool IsEmailExist(string email, bool close = true)   // test si un email existe dans la base de donnée
        {
            bool exist = false;
            string req = "SELECT email FROM contacts WHERE email=@email";
            using (SqlCommand cmd = new SqlCommand(req, GetConnection()))
            {
                cmd.Parameters.AddWithValue("@email", email);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        exist = true;
                    }
                }
            }
            CloseConnection(close);

            return exist;
        }

        public Contact FindByEmail(string email, bool close = true)    // Trouve un contact dans la bdd en fonction d'un email 
        {
            Contact contact = null;
            string req = "SELECT id,prenom,nom,date_naissance FROM contacts WHERE email=@email";
            using (SqlCommand cmd = new SqlCommand(req, GetConnection()))
            {
                cmd.Parameters.AddWithValue("@email", email);
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        contact = new Contact(reader.GetString(1), reader.GetString(2), email, reader.GetDateTime(3));
                        contact.Id = reader.GetInt64(0);
                    }
                }
            }
            CloseConnection(close);
            return contact;
        }
    }
}
