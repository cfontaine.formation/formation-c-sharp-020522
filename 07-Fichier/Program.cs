﻿using _07_BibliothequeFichier;
using System;
using System.Collections.Generic;

namespace _07_Fichier
{
    // Pour ajouter au projet la bibliothèque
    // Cliquer droit sur le projet -> Ajouter -> référence -> choisir 07-BibliothequeFichier
    internal class Program
    {
        static void Main(string[] args)
        {
            Utilitaire.InfoLecteur();
            Utilitaire.InfoDossier();
            Utilitaire.Parcourir(@"C:\Dawan\TestIo");
            Utilitaire.InfoFichier();

            Utilitaire.EcritureFichierText(@"C:\Dawan\TestIo\test1.txt");
            List<string> lst = Utilitaire.LectureFichierText(@"C:\Dawan\TestIo\test1.txt");
            foreach (string s in lst)
            {
                Console.WriteLine(s);
            }

            Utilitaire.EcrireFichierBin(@"C:\Dawan\Testio\test.bin");
            Utilitaire.LireFichierBin(@"C:\Dawan\Testio\test.bin");
            Utilitaire.Copie(@"C:\Dawan\TestIo\Logo dawan.png", @"C:\Dawan\TestIo\copie.png");
            Console.ReadKey();
        }
    }
}
