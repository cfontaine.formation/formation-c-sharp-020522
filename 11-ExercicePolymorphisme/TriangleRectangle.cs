﻿namespace _11_ExercicePolymorphisme
{
    internal class TriangleRectangle : Rectangle
    {
        public TriangleRectangle(double largeur, double longueur, Couleurs couleur) : base(largeur, longueur, couleur)
        {

        }

        public override double CalculSurface()
        {
            return base.CalculSurface() / 2.0;
        }
    }
}
