﻿using System;

namespace _11_ExercicePolymorphisme
{
    class Cercle : Forme
    {

        public double Rayon { get; set; }

        public Cercle(double rayon, Couleurs couleur) : base(couleur)
        {
            Rayon = rayon;
        }

        public override double CalculSurface()
        {
            return Math.PI * Math.Pow(Rayon, 2);
        }
    }
}
