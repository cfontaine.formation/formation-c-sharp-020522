﻿using System;
using System.Text;

namespace _04_Methodes
{
    struct Img
    {
        public byte[] pixel;
    }
    internal class Program
    {
        static int Main(string[] args)
        {
            // Appel de methode 
            double result = Multiplier(12.4, 4.56);
            Console.WriteLine(result);

            // Appel de methode (sans type retour)
            Afficher(result);

            // Exercice Maximum
            Console.WriteLine("Maximum: entrer 2 réels");
            double d1 = Convert.ToDouble(Console.ReadLine());
            double d2 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine(Maximum(d1, d2));

            // Exercice Paire
            Console.WriteLine("Paire: entrer un entier");
            int e = Convert.ToInt32(Console.ReadLine());
            if (Even(e))
            {
                Console.WriteLine("Nombre paire");
            }
            else
            {
                Console.WriteLine("Nombre impaire");
            }

            // Passage par valeur (par défaut en C#)
            // C'est une copie de la valeur du paramètre qui est transmise à la méthode
            int val = 2;
            Console.WriteLine(val);
            TestParamValeur(12);

            // Passage de paramètre par référence
            // La valeur de la variable passée en paramètre est modifiée, si elle est modifiée dans la méthode
            TestParamRef(ref val);
            Console.WriteLine(val);

            // Passage de paramètre en sortie
            // Un paramètre out ne sert à la méthode qu'à retourner une valeur
            // L'argument transmis doit référencer une variable ou un élément de tableau
            // La variable n'a pas besoin d'être initialisée

            // int o1;
            // double o2;

            // On peut déclarer les variables de retours dans les arguments pendant l'appel de la méthode
            TestParamOut(out int o1, out double o2);
            Console.WriteLine(o1 + " " + o2);

            // On peut ignorer un paramètre out en le nommant _
            TestParamOut(out _, out double o3);
            Console.WriteLine(o3);

            // Paramètre optionnel
            TestParamOptionnel(10);
            TestParamOptionnel(34, "Hello");
            TestParamOptionnel(78, "World", false);

            // Paramètres nommés
            TestParamOptionnel(1, b: false);
            TestParamOptionnel(b: false, i: 5, str: "ert");

            // Nombre de paramètres variable
            Console.WriteLine(Moyenne());
            Console.WriteLine(Moyenne(5, 6, 7, 8));
            // Passage d'un tableau en paramètre
            int[] tab = { 1, 5, 7, 4 };
            Console.WriteLine(Moyenne(tab));

            StringBuilder str = new StringBuilder("azerty");
            TestParamValeurObjet(str);
            Console.WriteLine(str);

            TestParamValeurObjModifEtat(str);
            Console.WriteLine(str);

            // Exercice Echange
            Console.WriteLine("Echange: saisir 2 entiers");
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"{a}  {b}");
            Swap(ref a, ref b);
            Console.WriteLine($"{a}  {b}");

            // Exercice Tableau
            Menu();

            // Surcharge de méthode
            // Correspondance exacte des type des paramètres
            Console.WriteLine(Somme(1, 2));
            Console.WriteLine(Somme(1.5, 2.6));
            Console.WriteLine(Somme(10, 20.6));
            Console.WriteLine(Somme("az", "erty"));

            // Pas de correspondance exacte => convertion automatique
            Console.WriteLine(Somme(1L, 2L));   // => appel de la méthode avec 2 double en paramètres
            Console.WriteLine(Somme('a', 'e')); // => appel de la méthode avec 2 entiers en paramètres
            Console.WriteLine(Somme(20.6, 10)); // => appel de la méthode avec 2 double en paramètres

            // Pas de conversion possible => Erreur
            // Console.WriteLine(Somme(20.6M, 10));

            // Paramètre de la méthode Main
            // En lignes de commandes: 05-methode.exe Azerty 12234 "aze rty"
            // Dans visual studio: propriétés du projet-> onglet Déboguer-> options de démarrage -> Arguments de la ligne de commande
            foreach (var arg in args)
            {
                Console.WriteLine(arg);
            }

            // Méthode récursive
            int fact = Factorial(3);
            Console.WriteLine(fact);

            // Corps d'expression
            Console.WriteLine(Multiplication(1, 5));

            // Méthode locale
            AffSquare(6);

            // Passage de paramètre référence en entrée => in
            Img img;
            img.pixel = new byte[1000];
            TestParamIn(img);
            Console.ReadKey();

            return 0;
        }

        // Déclaration
        static double Multiplier(double a, double b)
        {
            return a * b;   // L'instruction return
                            // - Interrompt l'exécution de la méthode
                            // - Retourne la valeur de l'expression à droite
        }

        static void Afficher(double d)   // void => pas de valeur retourné
        {
            Console.WriteLine(d);
            // avec void => return; ou return peut être omis ;
        }

        #region Exercice Maximum
        // Ecrire une méthode Maximum qui prends en paramètre 2 nombres et elle retourne le maximum
        static double Maximum(double v1, double v2)
        {
            //if(v1 > v2)
            //{
            //    return v1;
            //}
            //else
            //{
            //    return v2;
            //}
            return v1 > v2 ? v1 : v2;
        }
        #endregion

        #region Exercice Paire
        // Écrire une méthode Paire qui prend un entier en paramètre un entier
        // Elle retourne vrai, si il est paire
        static bool Even(int v)
        {
            //if (v % 2 == 0)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
            return (v & 0b1) == 0;//v % 2 == 0;
        }
        #endregion

        #region Passage de paramètre

        // Passage par valeur
        static void TestParamValeur(int a)
        {
            Console.WriteLine(a);
            a = 42;  // La modification de la valeur du paramètre a n'a pas d'influence en dehors de la méthode
            Console.WriteLine(a);
        }

        // Passage par référence => ref
        static void TestParamRef(ref int a)
        {
            Console.WriteLine(a);
            a = 42;
            Console.WriteLine(a);
        }

        // Passage de paramètre en sortie => out
        static void TestParamOut(out int a, out double b)
        {
            // int tmp = a;  // erreur, on ne peut pas accéder en entrée à un paramètre out 
            a = 42;
            int tmp = a;
            b = 3;          // La méthode doit obligatoirement affecter une valeur aux paramètres out
        }

        // On peut donner une valeur par défaut aux arguments passés par valeur
        static void TestParamOptionnel(int i, string str = "azerty", bool b = true)
        {
            Console.WriteLine($"i={i} str={str} b={b}");
        }

        // Nombre d'arguments variable => params
        static double Moyenne(params int[] vp)
        {
            double somme = 0.0;
            foreach (var elm in vp)
            {
                somme += elm;
            }
            return somme / (vp.Length);
        }

        // Passage par valeur d'un objet

        // La référence de l'objet passé en paramètre est copiée dans la référence sb  
        static void TestParamValeurObjet(StringBuilder sb)
        {
            Console.WriteLine(sb);
            sb = new StringBuilder("modif");    // On crée un nouveau objet = la référence dans sb change
            Console.WriteLine(sb);
        } // comme sb est une copie, la nouvelle référence est détruite => pas de modification de l'objet passé en paramètre

        static void TestParamValeurObjModifEtat(StringBuilder sb)
        {
            Console.WriteLine(sb);
            sb.Append(" Modif"); // On modifie l'état de l'objet et pas la référence => l'objet passé en paramètre sera modifié
            Console.WriteLine(sb);
        }

        // in => Passage par référence, mais uniquement en entrée
        // se comporte comme un passage par valeur, mais il n'y a pas de copie => gain en performance pour des structures (type valeur) contenant beaucoup de données 
        static void TestParamIn(in Img image)
        {
            // image.pixel = new byte[1000];
            image.pixel[0] = 4;
        }
        #endregion

        #region Exercice Echange
        // Écrire une méthode Swap qui prend en paramètre 2 entiers et qui permet d'inverser le contenu des 2 variables passées en paramètre
        static void Swap(ref int a, ref int b)
        {
            int tmp = a;
            a = b;
            b = tmp;
        }
        #endregion

        #region Exercice Tableau
        // - Écrire une méthode qui affiche un tableau d’entier

        // - Écrire une méthode qui permet de saisir :
        //   - La taille du tableau
        //   - Les éléments du tableau

        // - Écrire une méthode qui calcule :
        //   - le maximum
        //   - la moyenne

        // - Faire un menu qui permet de lancer ces méthodes
        //      1 - Saisir le tableau
        //      2 - Afficher le tableau
        //      3 - Afficher le maximum et la moyenne
        //      0 - Quitter
        //      Choix =
        static void AfficherTab(int[] tab)
        {
            Console.Write("[ ");
            foreach (var elm in tab)
            {
                Console.Write($"{elm} ");
            }
            Console.WriteLine(" ]");
        }

        static int[] SaisirTab()
        {
            Console.WriteLine("Taille du tableau");
            int size = Convert.ToInt32(Console.ReadLine());
            int[] t = new int[size];
            for (int i = 0; i < size; i++)
            {
                Console.Write($"t[{i}]=");
                t[i] = Convert.ToInt32(Console.ReadLine());
            }
            return t;
        }

        static void CalculTab(int[] t, out int maximum, out double moyenne)
        {
            maximum = t[0];
            double somme = t[0];
            for (int i = 1; i < t.Length; i++)
            {
                if (t[i] > maximum)
                {
                    maximum = t[i];
                }
                somme += t[i];
            }
            moyenne = somme / t.Length;
        }

        static void AfficherMenu()
        {
            Console.WriteLine("\n1 - Saisir le tableau");
            Console.WriteLine("2 - Afficher le tableau");
            Console.WriteLine("3 - Afficher le maximum et la moyenne");
            Console.WriteLine("0 - Quitter");
        }

        static void Menu()
        {
            int[] tab = null;
            int choix;
            do
            {
                AfficherMenu();
                choix = Convert.ToInt32(Console.ReadLine());
                switch (choix)
                {
                    case 1:
                        tab = SaisirTab();
                        break;
                    case 2:
                        if (tab != null)
                        {
                            AfficherTab(tab);
                        }
                        break;
                    case 3:
                        if (tab != null)
                        {
                            CalculTab(tab, out int max, out double moyenne);
                            Console.WriteLine($"maximum={max} moyenne={moyenne}");
                        }
                        break;
                    case 0:
                        Console.WriteLine("au revoir!");
                        break;
                    default:
                        Console.WriteLine("Choix incorrect");
                        break;
                }

            }
            while (choix != 0);
        }
        #endregion

        #region Surcharge de méthode
        // Une méthode peut être surchargée => plusieurs méthodes peuvent avoir le même nom, mais leurs signatures doient être différentes
        // La signature d'une méthode correspond aux types et nombre de paramètres
        // Le type de retour ne fait pas partie de la signature
        static int Somme(int a, int b)
        {
            Console.WriteLine("2 entiers");
            return a + b;
        }

        static double Somme(double a, double b)
        {
            Console.WriteLine("2 doubles");
            return a + b;
        }

        static double Somme(int a, double b)
        {
            Console.WriteLine("un entier et un double");
            return a + b;
        }

        static string Somme(string a, string b)
        {
            Console.WriteLine("2 chaines de caractères");
            return a + b;
        }
        #endregion

        // Récursivité: une méthode qui s'appelle elle-même
        static int Factorial(int n) // factoriel= 1* 2* … n
        {
            if (n <= 1) // condition de sortie
            {
                return 1;
            }
            else
            {
                return Factorial(n - 1) * n;
            }
        }

        // Corps de méthode
        // une méthode qui comprend une seule expression, peut s'écrire avec l'opérateur =>
        static int Multiplication(int a, int b) => a * b;

        // Méthode locale
        static void AffSquare(double v)
        {
            // Console.WriteLine(Square(v));

            // Une méthode locale n'est visible que par la méthode englobante
            // double Square(double a) => a * a;

            // Capture
            // Une méthode locale peut accéder aux variables locales et aux paramètres de la méthode englobante
            double w = v;
            Console.WriteLine(Square());
            double Square() => v * w;
        }
    }
}
