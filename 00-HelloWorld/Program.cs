﻿using System;

namespace _00_HelloWorld
{
    /// <summary>
    /// Classe HelloWorld
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Point d'entée du programme
        /// </summary>
        /// <param name="args">Arguments ligne de commande</param>
        static void Main(string[] args)
        {
            Console.WriteLine("hello World!"); // Commentaire fin de ligne
            /*
             * Commentaire 
             * sur 
             * plusieurs lignes
             */
            Console.ReadKey();
        }
    }
}
