﻿using _15_AdoDotNet;
using System;
using System.Text.RegularExpressions;
using System.Windows;

namespace _18_Annuaire_WPF
{
    /// <summary>
    /// Logique d'interaction pour ContactEditWindow.xaml
    /// </summary>
    public partial class ContactEditWindow : Window
    {

        public Contact ContactForm { get; private set; }
        public ContactEditWindow()
        {
            InitializeComponent();
        }

        public ContactEditWindow(Contact contact) : this()
        {
            if (contact != null)
            {
                ContactForm = contact;
                Title = "Modifier un Contact";
                BpAction.Content = "Modifer";
                PrenomTextBox.Text = contact.Prenom;
                NomTextBox.Text = contact.Nom;
                DateNaissancePicker.SelectedDate = contact.DateNaissance;
                EmailTextBox.Text = contact.Email;
            }
        }

        private bool CheckContactInfo(out string messageErr)
        {
            if (PrenomTextBox.Text == "")
            {
                messageErr = "Le champs prénom ne doit pa être vide";
                return false;
            }
            else if (NomTextBox.Text == "")
            {
                messageErr = "Le champs nom ne doit pa être vide";
                return false;
            }
            else if (!DateNaissancePicker.SelectedDate.HasValue)
            {
                messageErr = "La date de naissance ne doit pa être vide";
                return false;
            }
            else if (DateNaissancePicker.SelectedDate.Value > DateTime.Now)
            {
                messageErr = "La date de naissance n'est pas valide";
                return false;
            }
            else if (!Regex.IsMatch(EmailTextBox.Text, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
            {
                messageErr = "La email n'est pas valide";
                return false;
            }
            messageErr = "";
            return true;
        }

        private void OnClickBpAction(object sender, RoutedEventArgs e)
        {
            if (!CheckContactInfo(out string msgErr))
            {
                MessageBox.Show(msgErr, "Erreur de saisie", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                if (ContactForm == null)
                {
                    ContactForm = new Contact(PrenomTextBox.Text, NomTextBox.Text, EmailTextBox.Text, DateNaissancePicker.SelectedDate.Value);
                }
                else
                {
                    ContactForm.Prenom = PrenomTextBox.Text;
                    ContactForm.Nom = NomTextBox.Text;
                    ContactForm.DateNaissance = DateNaissancePicker.SelectedDate.Value;
                    ContactForm.Email = EmailTextBox.Text;
                    DialogResult = true;
                }

            }
        }
    }
}
