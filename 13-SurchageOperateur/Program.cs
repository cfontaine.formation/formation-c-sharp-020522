﻿using System;

namespace _13_SurchageOperateur
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Point a = new Point(1, 2);

            Point b = -a;
            Console.WriteLine(b);

            Point c = new Point(3, 4);
            Console.WriteLine(a + c);

            Point d = c * 4;
            Console.WriteLine(d);

            a += c;  // quand on surcharge un opérateur binaire, l'opérateur composé associé est aussi automatiquement surchargé

            Console.WriteLine(d == a);
            Console.WriteLine(d != a);

            #region Exercice surcharge opérateur
            Fraction f1 = new Fraction(1, 2);
            Console.WriteLine(f1.Calculer());
            Fraction f2 = new Fraction(1, 2);
            Console.WriteLine(f1 + f2);
            Console.WriteLine(f1 * f2);
            Console.WriteLine(f1 * 3);
            Console.WriteLine(f1 == f2);
            Console.WriteLine(f1 == (f2 * 3));
            #endregion
            Console.ReadKey();
        }
    }
}
