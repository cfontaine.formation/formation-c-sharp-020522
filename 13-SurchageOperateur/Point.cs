﻿using System;

namespace _13_SurchageOperateur
{


    internal class Point
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Point()
        {
        }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }


        public void Deplacer(int tx, int ty)
        {
            X += tx;
            Y += ty;
        }

        public double Norme()
        {
            return Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2));
        }

        public double Distance(Point p)
        {
            return Math.Sqrt(Math.Pow((p.X - X), 2) + Math.Pow((p.Y - Y), 2));
        }

        public static double Distance(Point p1, Point p2)
        {
            return Math.Sqrt(Math.Pow((p2.X - p1.X), 2) + Math.Pow((p2.Y - p1.Y), 2));
        }

        public override string ToString()
        {
            return $"({X}, {Y})";
        }

        // Surcharge opérateur unaire => un paramètre
        public static Point operator -(Point p)
        {
            return new Point(-p.X, -p.Y);
        }

        // Surcharge opérateur binaire => deux paramètres
        public static Point operator +(Point p1, Point p2)
        {
            return new Point(p1.X + p2.X, p1.Y + p2.Y);
        }

        // Au moins un paramètre doit être de type Point l'autre peut être un Point ou un autre type
        public static Point operator *(Point p, int scalaire)
        {
            return new Point(p.X * scalaire, p.Y * scalaire);
        }

        // Si on surcharge l'opérateur d'égalité on est obligé de surchargé celui d'inégalité, il faut aussi redéfinir les méthodes Equals et getHashCode
        public static bool operator ==(Point p1, Point p2)
        {
            return p1.X == p2.X && p1.Y == p2.Y;
        }

        public static bool operator !=(Point p1, Point p2)
        {
            return !(p1 == p2);
        }

        public override bool Equals(object obj)
        {
            return obj is Point point &&
                   X == point.X &&
                   Y == point.Y;
        }

        public override int GetHashCode()
        {
            int hashCode = 1861411795;
            hashCode = hashCode * -1521134295 + X.GetHashCode();
            hashCode = hashCode * -1521134295 + Y.GetHashCode();
            return hashCode;
        }

    }

}
