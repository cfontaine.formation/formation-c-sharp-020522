﻿using System;

namespace _13_SurchageOperateur
{
    // Exercice Surcharge opérateur
    // - + entre 2 Fractions(utiliser la méthode pgcd pour simplifier la fraction)
    // - *  entre 2 Fractions(utiliser la méthode pgcd pour simplifier la fraction)
    // - *  entre une Fractions et un entier(utiliser la méthode pgcd pour simplifier la fraction)
    // - == et != les opérateurs retournent un booléen et prend en paramètre 2 Fractions
    // - tostring

    class Fraction
    {
        public int Numerateur { get; set; }
        public int Denominateur { get; set; }

        public Fraction(int numerateur = 0, int denominateur = 1)
        {
            Numerateur = numerateur;
            if (denominateur == 0)
            {
                throw new ArithmeticException("Denominateur égale à 0");
            }
            Denominateur = denominateur;
        }

        public double Calculer()
        {
            return ((double)Numerateur) / Denominateur;
        }

        public static Fraction operator +(Fraction f1, Fraction f2)
        {
            int num = f1.Numerateur * f2.Denominateur + f2.Numerateur * f1.Denominateur;
            return FractionSimplifier(num, f1.Denominateur * f2.Denominateur);
        }

        public static Fraction operator *(Fraction f1, Fraction f2)
        {
            return FractionSimplifier(f1.Numerateur * f2.Numerateur, f1.Denominateur * f2.Denominateur);
        }
        public static Fraction operator *(Fraction f, int scal)
        {
            return FractionSimplifier(f.Numerateur * scal, f.Denominateur);
        }

        public static bool operator ==(Fraction f1, Fraction f2)
        {
            return f1.Numerateur == f2.Numerateur && f1.Denominateur == f2.Denominateur;
        }

        public static bool operator !=(Fraction f1, Fraction f2)
        {
            return !(f1 == f2);
        }

        public override string ToString()
        {
            return Denominateur != 1 ? $"{Numerateur}/{Denominateur}" : $"{Numerateur}";
        }

        public override bool Equals(object obj)
        {
            return obj is Fraction fraction &&
                   Numerateur == fraction.Numerateur &&
                   Denominateur == fraction.Denominateur;
        }

        public override int GetHashCode()
        {
            int hashCode = 1421187769;
            hashCode = hashCode * -1521134295 + Numerateur.GetHashCode();
            hashCode = hashCode * -1521134295 + Denominateur.GetHashCode();
            return hashCode;
        }
        private static Fraction FractionSimplifier(int numerateur, int denominateur)
        {
            int pgcd = Pgcd(numerateur, denominateur);
            return new Fraction(numerateur / pgcd, denominateur / pgcd);
        }

        private static int Pgcd(int a, int b)
        {
            while (a != b)
            {
                if (a > b)
                {
                    a -= b;
                }
                else
                {
                    b -= a;
                }
            }
            return a;
        }

    }

}
