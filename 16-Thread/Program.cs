﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace _16_Thread
{
    internal class Program
    {
        static int _sharedData;     // Donnée partagée
        static object _locker = new object(); // Pour lock, il faut une référence

        static void Main()
        {

            Thread.CurrentThread.Name = "Main";

            Console.WriteLine("Début de la méthode Main");

            #region Classe Thread
            // ThreadStart -> Délégué qui permet de créer Thread (Méthode sans paramètre)
            Thread tsp = new Thread(new ThreadStart(ThreadSansParam)); // Le constructeur de la classe Thread prend en paramètre un délégué
            tsp.Start(); //  La Méthode Start permet de démmarrer le Thread

            // ParamterizedThreadStart -> Délégué qui permet de créer Thread (méthode qui a un paramètre de "type" object)
            Thread tap = new Thread(new ParameterizedThreadStart(ThreadMethode));
            tap.Start(50);
            #endregion

            #region Propriété
            Thread t1 = new Thread(new ParameterizedThreadStart(ThreadMethode));
            t1.Name = "T1";    // La propriété Name permet de nommer le Thread
                               // t1.Priority = ThreadPriority.Lowest;  // La propriété Priority permet de modifier la prioritédu thread
                               // Highest => le thread est le + prioritaire, AboveNormal, Normal (par défaut), BelowNormal, Lowest => le thread est le - prioritaire
            Console.WriteLine($"état du Thread à la création = {t1.ThreadState}");    // La propriétée ThreadState contient l'état du Thread, au départ => Unstarted
            t1.Start(50);
            Console.WriteLine($"état du Thread après L'éxécution de la méthode Start= {t1.ThreadState}"); // Runnable ou running s'il est executé
            #endregion

            // Thread Main
            for (int i = 0; i < 40; i++)
            {
                Console.WriteLine($"Main - {i}");
                Thread.Sleep(50);
            }

            #region Méthode Abort
            // Un thread démarre avec l'execution de la méthode Start et finit lorsque la méthode associé est finie 
            // On peut arréter les Thread aussi avec la méthode Arbort
            try
            {
                t1.Abort();
                Console.WriteLine($"état du Thread après l'arrét du Thread avec La méthode Arbort= {t1.ThreadState}");
            }
            catch (ThreadAbortException e)
            {
                Console.WriteLine($"Problème Abort {e.StackTrace}");
            }
            #endregion

            #region Thread join
            // Join 
            // bloque tous les threads appelants jusqu'à ce que ce thread se termine
            Thread t2 = new Thread(new ParameterizedThreadStart(ThreadMethode));
            t2.Start();
            t2.Join(100);  // Attend que le thread t2 soit finit pour continuer

            // Thread Main
            for (int i = 0; i < 30; i++)
            {
                Console.WriteLine($"Main - {i}");
                Thread.Sleep(50);
            }
            #endregion

            #region Verrouiller un Thread
            Thread tlt = new Thread(new ThreadStart(ThreadLockTest));
            tlt.Name = "TLT";
            tlt.Start();
            ThreadLockTest();

            #endregion

            #region Task
            //
            // Task est une abstraction de niveau supérieur par rapport à un Thread (ajouté au Framework 4.0)
            // Elle représente une opération concurrente qui peut ou non être soutenue par un thread.

            // Run => permet de démmarrer une Task, elle prend en paramètre un deélégué Action
            Task task1 = Task.Run(() =>
            {
                Console.WriteLine("Début => task wait");
                Thread.Sleep(2000);
                Console.WriteLine("Fin => task wait");
            });
            Console.WriteLine($"Task wait={task1.IsCompleted}"); // IsCompleted => retoune True si la Task est finie
            // Wait => bloque  jusqu'à ce que la task se termine (équivalant d'un Join pour les threads)
            task1.Wait();
            #endregion

            #region Task<Result>
            // Task<TResult> permet à une Task de retourner une valeur
            Task<int> task2 = Task.Run(() =>
            {
                int res = 0;
                for (int i = 0; i < 10000; i++)
                {
                    res += i;
                }
                return res;
            });
            Console.WriteLine("Task running .....");
            // Bloque tant que la task2 n'est pas terminé et n'a pas retourné son resultat
            Console.WriteLine($"Task<TResult> Result={task2.Result}");
            #endregion

            #region Continuations
            Task<int> primeNumberTask = Task.Run(() =>
            {
                int res = 0;
                for (int i = 0; i < 10000; i++)
                {
                    res += i;
                }
                return res;
            });
            var awaiter = primeNumberTask.GetAwaiter();
            awaiter.OnCompleted(() =>
            {
                int result = awaiter.GetResult();
                Console.WriteLine(result);
            });
            #endregion

            #region Méthode asynchrone
            LaunchAsynchrone();
            #endregion

            #region Timer
            Timer t = new Timer(testTimer, "Message du timer",
            0,          //appel tout de suite
            1000);      //et toutes les secondes
            #endregion

            #region Processus
            Process p2 = Process.Start("Notepad.exe");
            #endregion
            Console.WriteLine("Fin de la méthode Main");
            Console.ReadKey();
        }
        static void ThreadSansParam()
        {
            for (int i = 0; i < 30; i++)
            {
                Console.WriteLine($"Thread sans paramètre i={i}");
            }
        }

        static void ThreadMethode(object time)
        {
            Thread current = Thread.CurrentThread;  // propriété statique qui permet d'accéder au thread en cours
            Console.WriteLine($"Début du Thread {current.Name} {current.Priority} {time}ms ");
            for (int i = 0; i < 30; i++)
            {
                Console.WriteLine($"Thread {current.Name} i={i}");
                if (time is int t)
                {
                    Thread.Sleep(t);   // Pour suspendre le thread actuel pendant les t millisecondes
                }
            }
            Console.WriteLine($"Fin du Thread {current.Name}");

        }

        static void ThreadLockTest()
        {
            lock (_locker)
            {
                if (_sharedData == 0)
                {
                    ++_sharedData;
                }
                Console.WriteLine($"\tLock-{Thread.CurrentThread.Name}-{_sharedData}");
            }
        }

        static async Task TestAsynchrone()
        {
            await Task.Delay(3000);
            int val = 42;
            Console.WriteLine(val);
        }

        static async void LaunchAsynchrone()
        {
            await TestAsynchrone();
            Console.WriteLine("Calcul terminé");
        }

        static void testTimer(object state)
        {
            Console.WriteLine(state);
        }



    }
}