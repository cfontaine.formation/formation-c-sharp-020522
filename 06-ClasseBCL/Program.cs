﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _06_ClasseBCL
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region Chaine de carctères
            string hello = "Hello"; // littéral;
            Console.WriteLine(hello);
            string str = new string('a', 10);
            Console.WriteLine(str);
            Console.WriteLine("Hello".ToLower());

            // Length -> Nombre de caractère de la chaine de caractère
            Console.WriteLine(str.Length); // 10

            // On peut accèder à un caractère de la chaine comme à un élément d'un tableau (index commence à 0)
            Console.WriteLine(hello[1]);

            // Concaténation 
            // avec l'opérateur + 
            string str2 = hello + " World";
            Console.WriteLine(str2);
            // ou avec la méthode de classe Concat
            string str3 = string.Concat(hello, " world");
            Console.WriteLine(str3);

            // La méthode Join concatène les chaines, en les séparants par une chaine de séparation
            string str4 = string.Join(";", "azerty", "uiop", "dfgh", "jklm");
            Console.WriteLine(str4);

            // Découpe la chaine en un tableau de sous-chaine suivant un ou plusieurs séparateur, par défaut le caractère espace
            string[] tabS = str4.Split(';');
            foreach (string elm in tabS)
            {
                Console.WriteLine(elm);
            }

            // Substring permet d'extraire une sous-chaine d'une chaine
            // de l'indice passé en paramètre juqu'à la fin de la chaine
            Console.WriteLine(str3.Substring(6));
            // ou pour le nombre de caractères passé en paramètre
            Console.WriteLine(str3.Substring(6, 2));

            // Insérer une sous-chaine à partir d'un indice
            Console.WriteLine(str3.Insert(5, "------"));

            // Supprimer un nombre de caractère à partir d'un indice
            Console.WriteLine(str3.Remove(5, 4));

            // StartsWith retourne true si la chaine commence par la chaine passé en paramètre
            Console.WriteLine(str3.StartsWith("Hell")); // true
            Console.WriteLine(str3.StartsWith("aaa")); //false

            // IndexOf retourne la première occurence du caractère ou de la chaine passée en paramètre    
            Console.WriteLine(str3.IndexOf("o")); // 4
            Console.WriteLine(str3.IndexOf("o", 5)); // 7   idem mais à partir de l'indice passé en paramètre
            Console.WriteLine(str3.IndexOf("o", 8)); // -1  -1 retourne -1, si le caractère n'est pas trouvé

            int ps = -1;
            while (true)
            {
                ps = str3.IndexOf("o", ps + 1);
                if (ps == -1)
                {
                    break;
                }
                Console.WriteLine(ps);
            }

            // Remplace toutes les chaines (ou caratères) oldValue par newValue
            Console.WriteLine(str3.Replace('o', 'a'));

            // Retourne True si la sous-chaine passée en paramètre est contenu dans la chaine
            Console.WriteLine(str3.Contains("wo")); // true
            Console.WriteLine(str3.Contains("aaa")); // false

            // Aligne les caractères à droite en les complétant par un caractère à gauche pour une longueur spécifiée
            Console.WriteLine(str3.PadLeft(40, '_'));

            // Aligne les caractères à gauche en les complétant par un caractère à droite pour une longueur spécifiée
            Console.WriteLine(str3.PadRight(40, '_'));

            // Trim supprime les caractères de blanc du début et de la fin de la chaine
            string str5 = "      \n \t \t azerty  yuiop\n  \t  \n";
            Console.WriteLine(str5.Trim());
            Console.WriteLine(str5.TrimStart());    // idem mais uniquement au début de la chaine
            Console.WriteLine(str5.TrimEnd());      // idem mais uniquement à la fin de la chaine

            // ToUpper convertie tous les caractères en majuscule
            Console.WriteLine(str3.ToUpper());

            // ToLower convertie tous les caractères en minuscule
            Console.WriteLine(str3.ToLower());

            // égalité de 2 chaines  == ou equals
            Console.WriteLine("Hello" == hello);
            Console.WriteLine("Hello".Equals(hello));

            // Comparaison 0-> égale, 1-> se trouve après, -1 -> se trouve avant dans l'ordre alphabétique
            // Il existe 2 méthodes: une d'instance et de classe
            Console.WriteLine(string.Compare(hello, "bonjour")); //1
            Console.WriteLine("bonjour".CompareTo(hello)); // -1

            // On peut chainer l'appel des différentes méthodes
            Console.WriteLine(str3.ToUpper().Trim().Substring(5).ToLower());

            string str6 = "bonjour";
            string bonjour = str6.Substring(0, 1).ToUpper() + str6.Substring(1);
            Console.WriteLine(bonjour);

            // Lorsqu'il y a beaucoup de manipulation (+de 3) sur une chaine ( concaténation et insertion, remplacement,supression de sous-chaine)
            // il vaut mieux utiliser un objet StringBuilder qui est plus performant qu'un objet String => pas de création d'objet intermédiare
            StringBuilder sb = new StringBuilder("test");
            sb.Append("____");
            sb.Append(123);
            sb.Remove(4, 2);
            string str7 = sb.ToString();
            Console.WriteLine(str7);

            // Exercice Chaine de caractère
            // Inverse
            Console.WriteLine(Inverser("Bonjour"));
            // Palindrome
            Console.WriteLine(Palindrome("Bonjour"));
            Console.WriteLine(Palindrome("Radar"));
            // Accronyme
            Console.WriteLine(Acronyme("Comité international olympique"));
            Console.WriteLine(Acronyme("Organisation du traité de l'Atlantique Nord"));
            #endregion

            #region Date
            DateTime d = DateTime.Now;   // DateTime.Now => récupérer l'heure et la date courante
            Console.WriteLine(d);

            Console.WriteLine(d.Year);
            Console.WriteLine(d.Day);
            Console.WriteLine(d.Hour);

            DateTime dUtc = DateTime.UtcNow;
            Console.WriteLine(dUtc);

            DateTime vacance = new DateTime(2022, 7, 1);
            Console.WriteLine(vacance);

            TimeSpan duree = vacance - d;
            Console.WriteLine(duree);
            Console.WriteLine(duree.TotalHours);

            TimeSpan dixJour = new TimeSpan(10, 0, 0, 0);

            Console.WriteLine(d + dixJour);
            Console.WriteLine(d.Add(dixJour));
            Console.WriteLine(d.AddMonths(3));

            Console.WriteLine(d.CompareTo(vacance)); // -1
            Console.WriteLine(DateTime.Compare(vacance, d)); // 1

            // DateTime -> string
            Console.WriteLine(d.ToLongDateString());
            Console.WriteLine(d.ToLongTimeString());
            Console.WriteLine(d.ToShortDateString());
            Console.WriteLine(d.ToShortTimeString());

            Console.WriteLine(d.ToString("dddd MM yyyy"));

            // string -> DateTime
            DateTime d2 = DateTime.Parse("2022/05/10T17:00:00");
            Console.WriteLine(d2);
            #endregion

            #region Collection
            // Collection faiblement typé => elle peut contenir tous types d'objets
            ArrayList lst = new ArrayList();
            lst.Add("hello");
            lst.Add("world");
            lst.Add(3.4);
            if (lst[0] is string)
            {
                string str8 = (string)lst[0];
                Console.WriteLine(str8);
            }

            // Collection fortement typée => type générique
            List<string> lstStr = new List<string>();
            lstStr.Add("hello");
            // lstStr.Add(12);  // On ne peut plus ajouter que des chaines de caractères => sinon erreur de compilation
            lstStr.Add("World");
            lstStr.Add("Bonjour");
            lstStr.Add("A supprimer");
            Console.WriteLine(lstStr[0]);       // Accès à un élément de la liste
            Console.WriteLine(lstStr.Count);    // Nombre d'élément de la collection
            Console.WriteLine(lstStr.Max());    // Valeur maximum stocké dans la liste
            Console.WriteLine(lstStr.Min());
            lstStr[0] = "azerty";               // Accès à un élément de la liste

            foreach (var s in lstStr)           // Parcourir la collection complétement
            {
                Console.WriteLine(s);
            }
            lstStr.Reverse();                   // Inverser l'ordre de la liste

            foreach (var s in lstStr)
            {
                Console.WriteLine(s);
            }

            // Supprimer des éléments de la liste
            lstStr.RemoveAt(3); // à partir de l'indice
            lstStr.Remove("World"); //à partir de l'objet

            // Parcourir un collection avec un Enumérateur
            var it = lstStr.GetEnumerator();
            while (it.MoveNext())
            {
                Console.WriteLine(it.Current);
            }

            // IEnumerable -> Parcours d'une chaine
            string str9 = "azerty";
            foreach (var c in str9)
            {
                Console.WriteLine(c);
            }

            // IEnumerable
            var itN = Numbers().GetEnumerator();
            itN.MoveNext();
            Console.WriteLine(itN.Current);
            itN.MoveNext();
            Console.WriteLine(itN.Current);

            foreach (var e in Numbers())
            {
                Console.WriteLine(e);
            }

            // Dictionary => association clé/valeur
            Dictionary<int, string> m = new Dictionary<int, string>();
            m.Add(123, "Azerty");   // Add => ajout d'un valeur associé à une clé
            m.Add(12, "Hello");
            m.Add(356, "World");
            m.Add(45, "A supprimer");

            // Accès à un élément m[clé] => valeur
            m[12] = "Bonjour";  // modifier une valeur associé à la clé
            // On ne peut pas ajouter, si la clé éxiste déjà => exception
            // m.Add(123, "Qwerty");
            Console.WriteLine(m[356]);
            Console.WriteLine(m.Count);
            m.Remove(45);

            // Parcourir un dictionnary
            foreach (var kv in m)
            {
                Console.WriteLine($"{kv.Key} {kv.Value}");
            }

            // Contains -> tester l'existance d'une clé ou d'un valeur
            Console.WriteLine(m.ContainsKey(33));
            Console.WriteLine(m.ContainsValue("Bonjour"));
            m.Contains(new KeyValuePair<int, string>(123, "Azerty"));

            // Obtenir et parcourir toutes les clés du dictionary
            var keys = m.Keys;
            foreach (var k in keys)
            {
                Console.WriteLine(k);
            }

            // Stack => Pile FILO
            Stack<int> st = new Stack<int>();
            st.Push(3);     // Ajouter un élément
            st.Push(5);
            st.Push(89);
            st.Push(123);
            Console.WriteLine(st.Count);    // Nombre d'élément dans la pile
            Console.WriteLine(st.Peek());   // Peek => Lire l'élément en tête de la pile sans le retirer
            Console.WriteLine(st.Peek());
            Console.WriteLine(st.Pop());    // Pop => Lire l'élément en tête de la pile et le retire de la pile 
            Console.WriteLine(st.Pop());
            #endregion

            #region Méthode générique
            string sta = "aze";
            string stb = "rty";
            //Swap<string>(ref sta, ref stb);
            Swap(ref sta, ref stb); // Le type générique est déduit du type des paramètres => string

            int ia = 2;
            int ib = 45;
            Swap(ref ia, ref ib);
            Console.WriteLine($"{ia} {ib}");
            #endregion

            Console.ReadKey();
        }

        #region Exercice Inversion de chaine
        // Écrire la méthode Inverser qui prend en paramètre une chaine et qui retourne la chaine avec les caractères inversés
        // exemple : bonjour => ruojnob
        static string Inverser(string str)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = str.Length - 1; i >= 0; i--)
            {
                sb.Append(str[i]);
            }
            return sb.ToString();
        }
        #endregion

        #region Exercice Palindrome
        // Écrire une méthode qui accepte en paramètre une chaine et qui retourne un booléen pour indiquer si c'est palindrome
        // exemple :: SOS, radar
        static bool Palindrome(string str)
        {
            string tmp = str.ToLower();
            return tmp == Inverser(tmp);
        }
        #endregion

        #region Acronyme
        // Faire une méthode de classe qui prend en paramètre une phrase et qui retourne un acronyme       
        // Les mots de la phrase sont pris en compte s'ils contiennent plus de 2 lettres

        // Ex: Comité international olympique → CIO
        //    Organisation du traité de l'Atlantique Nord → OTAN
        static string Acronyme(string str)
        {
            StringBuilder sb = new StringBuilder();
            string[] elm = str.ToUpper().Trim().Split(' ', '\'');
            foreach (string e in elm)
            {
                if (e.Length > 2)
                {
                    sb.Append(e[0]);
                }
            }
            return sb.ToString();
        }
        #endregion

        static IEnumerable<int> Numbers()
        {
            for (int i = 0; i < 10; i++)
            {
                yield return i;
            }
        }

        static void Swap<T>(ref T a, ref T b)
        {
            T tmp = a;
            a = b;
            b = tmp;
        }
    }
}
