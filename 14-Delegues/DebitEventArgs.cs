﻿using System;

namespace _14_Delegues
{
    internal class DebitEventArgs : EventArgs
    {
        public double Solde { get; set; }

        public DebitEventArgs(double solde)
        {
            Solde = solde;
        }
    }
}
