﻿using System;

namespace _14_Delegues
{
    internal class CompteBancaire
    {
        public double Solde { get; set; }

        // La classe qui contient l’évènement doit d’abord déclarer un délégué
        // public delegate void BalanceDelegate();

        // l’évènement sera du même type que ce délégué
        // Par convention, le nom d’un évènement est de la forme On<Nom de l’évènement>

        // Déclaration de l'évènement
        // public event BalanceDelegate OnBalance;

        // Ou, on peut déclarer l'événement en utilisant le délégué EventHandler fournit par le Framework.NET
        public event EventHandler OnBalance;
        public CompteBancaire(double solde)
        {
            Solde = solde;
        }

        public void Debit(double montant)
        {
            Solde -= montant;
            if (Solde < 0.0)
            {
                OnBalance(this, new DebitEventArgs(Solde)); // S'il n'y a pas de paramètre => EventArgs.Empty
            }
        }

    }
}
