﻿using System;

namespace _14_Delegues
{
    internal class Program
    {
        // Prototype
        public delegate int Operation(int a, int b);
        public delegate bool Comparaison(int a, int b);

        // C# 1.0
        // ---------------------
        public static int Ajouter(int n1, int n2)
        {
            return n1 + n2;
        }

        public static int Mutilplier(int a, int b)
        {
            return a * b;
        }
        // ---------------------
        static void Main(string[] args)
        {
            #region delegate
            // C# 1.0
            int res = Calcul(2, 3, new Operation(Ajouter));
            Console.WriteLine(res);
            res = Calcul(2, 3, new Operation(Mutilplier));
            Console.WriteLine(res);

            // C# 2.0
            Operation oprAdd = delegate (int a, int b) { return a + b; };
            res = Calcul(4, 5, oprAdd);
            res = Calcul(4, 6, delegate (int a, int b) { return a * b; });

            // C# 3.0
            // res=Calcul(6,7,(int n1,int n2) => { return n1 + n2; });
            res = Calcul(6, 7, (n1, n2) => n1 + n2);
            Console.WriteLine(res);
            res = Calcul(6, 7, (n1, n2) => n1 * n2);
            Console.WriteLine(res);
            Console.WriteLine(CalculF(1, 2, (x, y) => x + y));

            // Délégués Func
            double d = 10.0;
            Func<double, double> multi = x => x * d;
            Console.WriteLine(multi(2.0));
            #endregion

            #region Exercice Delegate
            int[] tab = { 2, 6, 1, 7, 3, 9, 0 };
            SortTab(tab, (x, y) => x < y);
            foreach (int v in tab)
            {
                Console.WriteLine(v);
            }
            #endregion

            #region Evénement
            CompteBancaire cb = new CompteBancaire(400.0);
            cb.OnBalance += EmailDebitAlert;
            cb.OnBalance += SmsDebitAlert;
            cb.Debit(50.0);
            Console.WriteLine(cb.Solde);
            cb.Debit(500.0);
            #endregion
            Console.ReadKey();
        }

        public static void EmailDebitAlert(object o, EventArgs args)
        {
            DebitEventArgs debit = args as DebitEventArgs;
            CompteBancaire cb = o as CompteBancaire;
            Console.WriteLine("Email Solde <0");
            Console.WriteLine(debit.Solde);
            Console.WriteLine(cb.Solde);
        }

        public static void SmsDebitAlert(object o, EventArgs args)
        {
            Console.WriteLine("Sms Solde <0");
        }

        public static int Calcul(int c1, int c2, Operation op)
        {
            return op(c1, c2);
        }

        public static int CalculF(int a, int b, Func<int, int, int> op)
        {
            return op(a, b);
        }

        static void SortTab(int[] tab, Func<int, int, bool> cmp)
        // static void SortTab(int[] tab, Comparaison cmp)
        {
            bool notDone = true;
            int end = tab.Length - 1;
            while (notDone)
            {
                notDone = false;
                for (int i = 0; i < end; i++)
                {
                    if (cmp(tab[i], tab[i + 1]))
                    {
                        int tmp = tab[i];
                        tab[i] = tab[i + 1];
                        tab[i + 1] = tmp;
                        notDone = true;
                    }
                }
                end--;
            }
        }

    }
}
