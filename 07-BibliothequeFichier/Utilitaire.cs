﻿using System;
using System.Collections.Generic;
using System.IO;

namespace _07_BibliothequeFichier
{
    public class Utilitaire
    {
        public static void InfoLecteur()
        {
            // DriveInfo fournit des informations sur les lecteurs d'une machine
            DriveInfo[] lecteurs = DriveInfo.GetDrives();
            foreach (DriveInfo l in lecteurs)
            {
                Console.WriteLine(l.Name);              // Nom du lecteur
                Console.WriteLine(l.TotalSize);         // Espace total du lecteur
                Console.WriteLine(l.TotalFreeSpace);    // Espace disponible sur le lecteur
                Console.WriteLine(l.DriveType);         // Espace total du lecteur
                Console.WriteLine(l.DriveFormat);       // Système de fichiers du lecteur NTFS, FAT ...
            }
        }

        public static void InfoDossier()
        {
            //  Teste si le dossier existe
            if (Directory.Exists(@"C:\Dawan\TestIo"))
            {
                // Liste les répertoires contenu dans le chemin
                string[] dir = Directory.GetDirectories(@"C:\Dawan\TestIo");
                foreach (var d in dir)
                {
                    Console.WriteLine(d);
                }
                // Liste les fichiers du répertoire
                string[] files = Directory.GetFiles(@"C:\Dawan\Testio\");
                foreach (var p in files)
                {
                    Console.WriteLine(p);
                }

            }
            else
            {
                // Création du répertoire
                Directory.CreateDirectory(@"C:\Dawan\TestIo");
            }
        }

        public static void Parcourir(string chemin)
        {
            if (Directory.Exists(chemin))
            {
                string[] fileNames = Directory.GetFiles(chemin);
                foreach (var f in fileNames)
                {
                    Console.WriteLine(f);
                }
                string[] directoryNames = Directory.GetDirectories(chemin);
                foreach (var d in directoryNames)
                {
                    Console.WriteLine($"Reperoire: {d}");
                    Console.WriteLine("_________________");
                    Parcourir(d);
                }
            }
        }

        public static void InfoFichier()
        {
            // File.Encrypt(@"C:\Dawan\TestIo\testcrypt.txt");
            // File.Decrypt(@"C:\Dawan\TestIo\testcrypt.txt");

            // Teste si le fichier
            if (File.Exists(@"C:\Dawan\TestIo\asup.txt"))
            {
                // Supprime le fichier
                File.Delete(@"C:\Dawan\TestIo\asup.txt");
            }
            // Obtenir la taille d'un fichier
            FileInfo fi = new FileInfo(@"C:\Dawan\TestIo\testcrypt.txt");
            Console.WriteLine(fi.Length);
        }

        public static void EcritureFichierText(string chemin)
        {
            StreamWriter sw = null; // StreamWriter Ecrire un fichier texte
            try
            {
                sw = new StreamWriter(chemin, true);    // append à true => compléte le fichier s'il existe déjà, à false le fichier est écrasé
                for (int i = 0; i < 10; i++)
                {
                    sw.WriteLine("Hello world");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                    sw.Dispose();
                }
            }
        }

        // Using => Équivalent d'un try / finally + Close()
        public static List<string> LectureFichierText(string chemin)
        {

            List<string> lst = new List<string>();
            using (StreamReader sr = new StreamReader(chemin))
            {
                while (!sr.EndOfStream)
                {
                    string tmp = sr.ReadLine();
                    lst.Add(tmp);
                }
            }
            return lst;
        }


        public static void EcrireFichierBin(string chemin)
        {
            using (FileStream fs = new FileStream(chemin, FileMode.Append)) // FileStream =>  permet de Lire/Ecrire un fichier binaire
            {
                for (byte b = 0; b < 100; b++)
                {
                    fs.WriteByte(b);    // Ecriture d'un octet dans le fichier
                }
            }
        }

        public static void LireFichierBin(string chemin)
        {
            byte[] t = new byte[10];
            using (FileStream fs = new FileStream(chemin, FileMode.Open))
            {
                int nb = 1;
                while (nb != 0)
                {
                    nb = fs.Read(t, 0, t.Length);   // Lecture de 10 octets au maximum dans le fichier, ils sont placés dans le tableau tab à partir de l'indice 0
                    for (int i = 0; i < nb; i++)    // Read => retourne le nombre d'octets lue dans le fichier
                    {
                        Console.Write($"{t[i]} ");
                    }
                    Console.WriteLine();
                }
            }
        }


        public static void Copie(string source, string target)
        {
            FileStream fi = null;
            FileStream fo = null;
            try
            {
                // FileStream =>  permet de Lire/Ecrire un fichier binaire
                byte[] t = new byte[100];
                fi = new FileStream(source, FileMode.Open);
                fo = new FileStream(target, FileMode.CreateNew);
                while (true)
                {
                    // Lit 100 octets au maximum dans le fichier (source) et les place dans le tableau à partir de l'indice 0
                    // Read => retourne le nombre d'octets lue dans le fichier
                    int size = fi.Read(t, 0, t.Length);
                    if (size == 0)
                    {
                        break;
                    }
                    // écrit dans le fichier (cible) les octets qui ont été lue
                    fo.Write(t, 0, size);
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (fi != null)
                {
                    fi.Close();
                    fi.Dispose();
                }
                if (fo != null)
                {
                    fo.Close();
                    fo.Dispose();
                }
            }
        }
    }
}
