﻿using System;

namespace _03_Tableaux
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region tableau à une dimension
            // Déclarer un tableau
            //double[] tab;
            //tab = new double[5];

            double[] tab = new double[5];

            // Valeur d'nitialisation des éléments du tableau
            // - entier -> 0
            // - double , float ou un decimal -> 0.0
            // - char -> '\u0000'
            // - boolean -> false
            //  -type référence  -> null

            // Accèder à un élément du tableau
            Console.WriteLine(tab[0]);
            tab[1] = 1.23;

            // Si l'on essaye d'accéder à un élément en dehors du tableau -> IndexOutOfRangeException
            // Console.WriteLine(tab[10]); 
            // tab[15] = 4.56;

            // Nombre d'élément du tableau
            Console.WriteLine($"Nombre élément={tab.Length}");

            // Parcourir complétement un tableau (avec un for) 
            for (int i = 0; i < tab.Length; i++)
            {
                Console.WriteLine($"tab[{i}]={tab[i]}");
            }

            // Parcourir complétement un tableau (foreach)
            // foreach (double elm in tab)  // ou
            foreach (var elm in tab)        //  var -> elm est de type double
            {
                Console.WriteLine(elm); // elm uniquement en lecture
                                        // elm = 4.6;           // => Erreur ,on ne peut pas modifier elm
            }

            // On peut utiliser une variable entière pour donner la taille du tableau
            int st = 3;
            string[] tabStr = new string[st];

            // Déclaration et initialisation
            char[] tabChr = { 'a', 'z', 'e', 'r' };
            foreach (var elm in tabChr)
            {
                Console.WriteLine(elm);
            }
            #endregion

            #region Tableau Multidimensions
            // Déclaration d'un tableau à 2 dimensions
            int[,] tab2D = new int[3, 2];

            // Accès à un élémént d'un tableau à 2 dimensions
            Console.WriteLine(tab2D[1, 0]);
            tab2D[0, 0] = 8;

            // Nombre d'élément du tableau
            Console.WriteLine(tab2D.Length); // 6

            // Nombre de dimension du tableau
            Console.WriteLine(tab2D.Rank);  // 2

            // Nombre de ligne du tableau
            Console.WriteLine(tab2D.GetLength(0));  // 3

            // Nombre de colonne du tableau
            Console.WriteLine(tab2D.GetLength(1));  // 2

            // Parcourir complétement un tableau à 2 dimension => for
            for (int i = 0; i < tab2D.GetLength(0); i++)
            {
                for (int j = 0; j < tab2D.GetLength(1); j++)
                {
                    Console.Write($"{tab2D[i, j]} ");
                }
                Console.WriteLine();
            }

            // Parcourir complétement un tableau à 2 dimension => foreach
            foreach (var e in tab2D)   // avec var la variable e aura le même type que le tableau
            {
                Console.WriteLine(e);
            }
            #endregion

            #region Tableau de tableau
            // Déclaration d'un tableau de tableau
            int[][] tabEsc = new int[4][];
            tabEsc[0] = new int[2];
            tabEsc[1] = new int[5];
            tabEsc[2] = new int[3];
            tabEsc[3] = new int[4];

            // Accès à un élément
            Console.WriteLine(tabEsc[1][4]);
            tabEsc[0][0] = 1234;

            // Nombre de ligne
            Console.WriteLine(tabEsc.Length);
            int nbElm = 0;
            for (int i = 0; i < tabEsc.Length; i++)
            {
                nbElm += tabEsc[i].Length;
                Console.WriteLine(tabEsc[i].Length);
            }
            Console.WriteLine($"nombre d'élément={nbElm}");

            //  Parcourir complétement un tableau de tableau => for
            for (int i = 0; i < tabEsc.Length; i++)
            {
                for (int j = 0; j < tabEsc[i].Length; j++)
                {
                    Console.Write($"{tabEsc[i][j]}\t");
                }
                Console.WriteLine();
            }

            //  Parcourir complétement un tableau de tableau => foreach
            foreach (int[] r in tabEsc)
            {
                foreach (int e in r)
                {
                    Console.Write($"{e}\t");
                }
                Console.WriteLine();
            }

            // Déclarer et initialiser un tableau de tableau
            int[][] tabEsc2 = new int[][] {
                                        new int[] { 1, 2, 3 },
                                        new int[] { 3, 4, 8, 1, 4 },
                                        new int[] { 2, 7 }
                                };
            foreach (int[] r in tabEsc2)
            {
                foreach (int e in r)
                {
                    Console.Write($"{e}\t");
                }
                Console.WriteLine();
            }
            #endregion
            #region Exercice tableau
            // 1 - Trouver la valeur maximale et la moyenne d’un tableau de 5 entier: -7, -6, -4, -8, -3
            // 2 - Modifier le programme pour faire la saisie de la taille du tableau et saisir les éléments du tableau

            //  int[] t ={ -7,-6,-4,-8,-3}; //1


            Console.WriteLine("Taille du tableau");
            int size = Convert.ToInt32(Console.ReadLine());
            int[] t = new int[size];
            for (int i = 0; i < size; i++)
            {
                Console.Write($"t[{i}]=");
                t[i] = Convert.ToInt32(Console.ReadLine());
            }

            int max = t[0];
            double somme = t[0];
            for (int i = 1; i < t.Length; i++)
            {
                if (t[i] > max)
                {
                    max = t[i];
                }
                somme += t[i];
            }
            Console.WriteLine($"maximum= {max} moyenne= {somme / t.Length}");
            #endregion
            Console.ReadKey();
        }
    }
}
