﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _08_Winform
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
         //   MessageBox.Show(this, "test", "titre", MessageBoxButtons.OK, MessageBoxIcon.Information); ;
            ListViewItem item = new ListViewItem(); //"John",0

            item.Text = "John";
            item.SubItems.Add("Doe");
            item.SubItems.Add("jdoe@dawan.fr");
            item.SubItems.Add("09.00.00.00.00");
            listView1.Items.Add(item);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
