﻿using System;

namespace _09_Poo
{
    internal class CompteBancaire
    {
        public double Solde { get; protected set; } = 50.0;
        public Personne Titulaire { get; set; }
        public string Iban { get; }

        static int _compteurCompte;

        public CompteBancaire()
        {
            _compteurCompte++;
            Iban = "fr-5962-0000-" + _compteurCompte;
        }

        public CompteBancaire(Personne titulaire) : this()
        {
            Titulaire = titulaire;
        }

        public CompteBancaire(double solde, Personne titulaire) : this(titulaire)
        {
            Solde = solde;
        }

        public void Afficher()
        {
            Console.WriteLine("______________");
            Console.WriteLine($"Solde ={Solde}");
            Console.WriteLine($"Iban ={Iban}");
            Titulaire?.Afficher();
        }

        public void Crediter(double valeur)
        {
            if (valeur > 0.0)
            {
                Solde += valeur;
            }
        }

        public void Debiter(double valeur)
        {
            if (valeur > 0.0)
            {
                Solde -= valeur;
            }
        }

        public bool EstPositif()
        {
            return Solde > 0.0;
        }
    }
}
