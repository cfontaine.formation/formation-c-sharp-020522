﻿using System;

namespace _09_Poo
{
    //internal sealed class Voiture // sealed => interdire l'héritage à partir de la classe
    internal class Voiture
    {
        // Variables d'instances => Etat
        // On n'a plus besoin de déclarer les variables d'instances marque,compteurKm, vitesse, elles seront générées automatiquement par les propriétées automatique
        // string marque;
        string _couleur = "Rouge";
        string _plaqueIma;
        // int vitesse;
        // int compteurKm=20; //=default

        // Propriété
        // Propriété auto-implémenté => la variable d'instance est générée par le compilateur
        public string Marque { get; }
        public int CompteurKm { get; set; } = 20;  // On peut donner un valeur par défaut à une propriétée (littéral, expression ou une fonction)
        public int Vitesse { get; protected set; }

        // Propriété C#1
        public string Couleur
        {
            get
            {
                return _couleur;
            }
            set
            {
                if (value != null)
                {
                    _couleur = value;
                }
            }
        }

        // Propriété C# 7.0
        public string PlaqueIma
        {
            get => _plaqueIma;
            set => _plaqueIma = value;
        }

        // Agrégation
        public Personne Proprietaire { get; set; }

        // Variable de classe
        // static int compteurVoiture;  // Remplacer par une propriétée static
        public static int CompteurVoiture { get; private set; }

        // Constructeurs => On peut surcharger le constructeur
        // Constructeur par défaut
        public Voiture()
        {
            Console.WriteLine("Constructeur Voiture par défaut");
            CompteurVoiture++;
        }

        // this() => Chainnage de constructeur : appel du constructeur par défaut
        public Voiture(string marque, string couleur, string plaqueIma) : this()
        {
            Console.WriteLine("Constructeur Voiture 3 paramètres");
            Marque = marque;
            _couleur = couleur;
            this._plaqueIma = plaqueIma;
        }

        // this(marque, couleur, plaqueIma) => Chainnage de constructeur : appel du constructeur  Voiture(string marque, string couleur, int vitesse)
        public Voiture(string marque, string couleur, string plaqueIma, int vitesse) : this(marque, couleur, plaqueIma)
        {
            Console.WriteLine("Constructeur Voiture 3 paramètres");
            Vitesse = vitesse;
        }

        public Voiture(string marque, Personne proprietaire, string couleur, string plaqueIma) : this(marque, couleur, plaqueIma)
        {
            Proprietaire = proprietaire;
        }

        // Destructeur
        //~Voiture()
        //{
        //    Console.WriteLine("Destructeur Voiture");
        //}


        // Exemple de constructeur static: appelé avant la création de la première instance ou le référencement d’un membre statique
        static Voiture()
        {
            Console.WriteLine("Constructeur statique Voiture");
        }

        // Méthodes d'instances => comportement
        public void Accelerer(int vAcc)
        {
            if (vAcc > 0)
            {
                Vitesse += vAcc;
            }
        }

        public void Freiner(int vFrn)
        {
            if (vFrn > 0)
            {
                Vitesse -= vFrn;
            }
        }

        public void Arreter()
        {
            Vitesse = 0;
        }

        public bool EstArreter()
        {
            return Vitesse == 0;
        }

        public virtual void Afficher()
        {
            Console.WriteLine($"[{Marque} {_couleur} {_plaqueIma}  {Vitesse} {CompteurKm}]");
            //   if (Proprietaire != null)
            //   {
            Proprietaire?.Afficher();
            //   }
        }

        // Méthode get et set en Java et C++
        //public string GetCouleur()
        //{
        //    return couleur;
        //}

        //public void SetCouleur(string couleur)
        //{
        //    this.couleur = couleur;
        //}

        // Méthode de classe
        public static void TestMethodeClasse()
        {
            Console.WriteLine("Méthode de classe");
            //Console.WriteLine(vitesse); // Dans une méthode de classe, on n'a pas accès à une variable d'instance
            // Freiner(40);  // ou une méthode d'instance
            Console.WriteLine(CompteurVoiture);
        }

        public static bool EgalVitesse(Voiture va, Voiture vb)
        {
            return va.Vitesse == vb.Vitesse;  // mais on peut accéder à une variable d'instance par l'intermédiare d'un objet passé en paramètre
        }
    }
}
