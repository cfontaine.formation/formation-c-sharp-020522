﻿using System;

namespace _09_Poo
{
    class Conteneur
    {
        string VariableInstance = "instance";

        static string VariableClasse = "classe";

        public void Test()
        {
            Element e = new Element();
            e.Afficher(this);
        }

        // classe Imbriqué
        class Element
        {
            public void Afficher(Conteneur c)
            {
                Console.WriteLine(VariableClasse);      // On a accès aux variables de classe de la classe Conteneur
                Console.WriteLine(c.VariableInstance);  // Pour accèder aux variables d'instance de la classe Conteneur
                                                        // Il faut passer un objet conteneur en paramètre
            }
        }
    }
}
