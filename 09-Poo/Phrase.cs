﻿using System;

namespace _09_Poo
{
    class Phrase
    {

        string[] _mots;

        public Phrase(params string[] mots)
        {
            _mots = mots;
        }

        // indexeur
        public string this[int index]
        {
            get
            {
                return _mots[index];
            }
            set
            {
                _mots[index] = value;
            }
        }

        // On peut déclarer plusieurs indexeurs, chacun avec des paramètres de différents types
        public int this[string mot]
        {
            get
            {
                for (int i = 0; i < _mots.Length; i++)
                {
                    if (_mots[i] == mot)
                    {
                        return i;
                    }
                }
                throw new IndexOutOfRangeException();
            }
        }

        public void Afficher()
        {
            foreach (string mot in _mots)
            {
                Console.Write($"{mot}");
            }
        }

    }
}
