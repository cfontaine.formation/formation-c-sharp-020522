﻿using System;

namespace _09_Poo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region Voiture
            // Appel d'une méthode de classe
            Voiture.TestMethodeClasse();

            // Appel d'une variable de classe
            Console.WriteLine($" Nb voiture={Voiture.CompteurVoiture}");

            // Instantiation de la classe Voiture
            Voiture v1 = new Voiture();

            Console.WriteLine($" Nb voiture={Voiture.CompteurVoiture}");

            // Accès à une variable d'instance
            // v1._vitesse = 10;

            // Accès à une propriété en ecriture (set)
            v1.Couleur = "Blanc";

            // Accès à une propriété en lecture (get)
            Console.WriteLine(v1.Couleur);
            Console.WriteLine(v1.Vitesse);

            // Appel d’une méthode d’instance
            v1.Accelerer(30);
            v1.Afficher();
            v1.Freiner(5);
            Console.WriteLine(v1.EstArreter());
            v1.Arreter();
            Console.WriteLine(v1.EstArreter());
            v1.Afficher();

            Voiture v2 = new Voiture();
            // v2._vitesse = 20;
            Console.WriteLine($" Nb voiture={Voiture.CompteurVoiture}");

            // Test de l'appel du destructeur
            v2 = null;  // En affectant, null à la références v2.Il n'y a plus de référence sur l'objet voiture
                        // Il sera détruit lors de la prochaine execution du garbage collector

            // Forcer l'appel le garbage collector
            // appel explicite du garbage collector (à éviter)
            //GC.Collect();

            Voiture v3 = null;
            v3?.Afficher();

            Voiture v4 = new Voiture("Ford", "Bleu", "Fr-1245-AB");
            Console.WriteLine($" Nb voiture={Voiture.CompteurVoiture}");
            v4.Afficher();

            // Appel de méthode de classe
            Console.WriteLine($"égalité vitesse v1 et v4={Voiture.EgalVitesse(v1, v4)}");

            // Initialiseur d'objet
            // Avec un initialiseur d'objet on a accès uniquement à ce qui est public
            Voiture v5 = new Voiture
            {
                CompteurKm = 2,
                //   Vitesse = 30,
                //   Marque = "honda"
            };
            v5.Afficher();
            #endregion

            #region Indexeur
            Phrase ph = new Phrase("Bonjour", "il", "fait", "beau", "aujourd'hui");
            Console.WriteLine(ph[1]);
            ph[0] = "bonjour";
            Console.WriteLine(ph["fait"]);
            // Console.WriteLine(ph["aaaaa"]);
            #endregion

            #region Classe statique
            //  Math m = new Math();    // ne peut pas être instanciée
            // ne peut pas contenir de constructeurs d’instances
            Console.WriteLine(Math.Abs(-5));    // contient uniquement des membres statiques
            #endregion

            #region  Méthode d'extension
            // ajouter des fonctionnalités à des classes existantes
            string str = "bOnJouR".Capitalize();
            Console.WriteLine(str);
            #endregion

            #region Classe partielle
            Form1 fo1 = new Form1(123);
            fo1.Afficher();
            #endregion

            #region Classe Imbriquée
            Conteneur c = new Conteneur();
            c.Test();
            #endregion

            #region Agrégation
            Personne per1 = new Personne("John", "Doe", "jd@dawan.com", new Adresse("46, rue des cannonier", "Lille", "59000"));
            Voiture v6 = new Voiture("Honda", per1, "Noir", "AZ-234-RT");
            v6.Afficher();
            #endregion

            #region Héritage
            VoiturePrioritaire vp = new VoiturePrioritaire(false);
            vp.Accelerer(10);
            vp.Afficher();
            vp.AllumerGyro();
            Console.WriteLine(vp.Gyro);

            VoiturePrioritaire vp2 = new VoiturePrioritaire("Toyota", "Jaune", "ER-5689-SX", true);
            vp2.Afficher();
            #endregion

            #region Exercice CompteBancaire
            CompteBancaire cb = new CompteBancaire(150.0, per1);
            //  cb.solde = 150.0;
            //cb.iban = "fr-5962-00000";
            //  cb.iban = "John Doe";

            cb.Crediter(200.0);
            cb.Afficher();
            cb.Debiter(400.0);
            Console.WriteLine(cb.EstPositif());
            cb.Afficher();
            Personne per2 = new Personne("Allan", "Smithee", "asmithee@dawan.com", new Adresse("1, rue Esquermoise", "Lille", "59000"));
            CompteBancaire cb2 = new CompteBancaire(250.0, per2);
            cb2.Afficher();
            Console.WriteLine(per2.Domicile.Ville);

            CompteEpargne ce1 = new CompteEpargne(1.5, per2);
            ce1.Afficher();
            ce1.CalculInteret();
            ce1.Afficher();
            #endregion

            #region Exercice Point
            Point a = new Point();
            a.Afficher();
            Point b = new Point(1, 3);
            b.Afficher();
            a.Deplacer(3, 3);
            Console.WriteLine(a.Norme());

            Console.WriteLine(Point.Distance(a, b));

            Console.WriteLine(a.Distance(b));
            #endregion

            Console.ReadKey();
        }
    }
}
