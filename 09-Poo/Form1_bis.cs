﻿using System;

namespace _09_Poo
{
    // Classe partielle répartie sur plusieurs fichiers
    internal partial class Form1
    {
        public Form1(int size)
        {
            Size = size;
        }

        public void Afficher()
        {
            Console.WriteLine(Size);
        }
    }
}
