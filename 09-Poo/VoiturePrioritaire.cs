﻿using System;

namespace _09_Poo
{
    internal class VoiturePrioritaire : Voiture // VoiturePrioritaire hérite de Voiture
    {
        public bool Gyro { get; private set; }

        // base => pour appeler le constructeur de la classe mère
        public VoiturePrioritaire(bool gyro) // : base() implicite
        {
            Console.WriteLine("Constructeur VoiturePrioritaire un paramètre");
            Gyro = gyro;
        }

        public VoiturePrioritaire(string marque, string couleur, string plaqueIma, bool gyro) : base(marque, couleur, plaqueIma)
        {
            Console.WriteLine("Constructeur VoiturePrioritaire 4 paramètres");
            Gyro = gyro;
        }

        public VoiturePrioritaire(string marque, string couleur, string plaqueIma, int vitesse) : base(marque, couleur, plaqueIma, vitesse)
        {
        }

        public void AllumerGyro()
        {
            Vitesse += 20;
            Gyro = true;
        }

        public void EteindreGyro()
        {
            Gyro = false;
        }

        // Redéfinition
        public override void Afficher()
        {
            base.Afficher();
            Console.WriteLine($"Gyro {Gyro}");
        }

        // Occultation => redéfinir une méthode d'une classe mère et à « casser » le lien vers la classe mère
        public new void Accelerer(int vAcc)
        {
            // base => pour appeler une méthode de la classe mère
            base.Accelerer(2 * vAcc);
        }

    }
}
