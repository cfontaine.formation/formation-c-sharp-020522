﻿using System;

namespace _09_Poo
{
    internal class Personne
    {
        public string Prenom { get; set; }

        public string Nom { get; set; }

        public string Email { get; set; }

        public Adresse Domicile { get; set; }

        public Personne(string prenom, string nom, string email, Adresse domicile)
        {
            Prenom = prenom;
            Nom = nom;
            Email = email;
            Domicile = domicile;
        }

        public void Afficher()
        {
            Console.WriteLine($"{Prenom} {Nom} {Email}");
            Domicile?.Afficher();
        }
    }
}
