﻿using _12_EspaceDeNoms.dao;
using _12_EspaceDeNoms.gui;

// Definir un Alias
using ConsoleSys = System.Console;

namespace _12_EspaceDeNoms
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Nom complet d'une classe namespace.classe
            _12_EspaceDeNoms.gui.Window win1 = new _12_EspaceDeNoms.gui.Window();
            // avec using _12_EspaceDeNoms.gui;
            Window win2 = new Window();

            UserDao userDao = new UserDao();

            // En cas de conflit de nom, il faut utiliser le nom complet des classe
            _12_EspaceDeNoms.gui.Console cnx1 = new _12_EspaceDeNoms.gui.Console();
            System.Console.WriteLine();

            // ou un Alias pour une des 2 classes
            Console cnx2 = new Console();
            ConsoleSys.WriteLine();
        }
    }
}

