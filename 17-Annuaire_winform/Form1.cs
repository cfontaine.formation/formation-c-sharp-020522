﻿using _15_AdoDotNet;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows.Forms;

namespace _17_Annuaire_winform
{
    public partial class Form1 : Form
    {
        ContactDao dao = new ContactDao();

        public Form1()
        {
            InitializeComponent();
            ContactDao.ChaineConnection = ConfigurationManager.ConnectionStrings["chsqlserver"].ConnectionString;
            InitListView(dao.FindAll());
        }

        private void ajouterBtn_Click(object sender, EventArgs e)
        {
            Contact c = SaisirContact("Ajouter un contact");
            if (c != null)
            {
                dao.SaveOrUpdate(c);
                InitListView(dao.FindAll());
            }
        }

        private void modifierBtn_Click(object sender, EventArgs e)
        {
            if (SelectContact(out Contact cs, "Modifier un contact"))
            {
                Contact c = SaisirContact("Modifier un contact",cs);
                if (c != null)
                {
                    dao.SaveOrUpdate(c);
                    InitListView(dao.FindAll());
                }
            }
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            if (SelectContact(out Contact c, "Effacer un contact"))
            {
                dao.Delete(c);
                InitListView(dao.FindAll());
            }
        }

        private void quitBtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        private void InitListView(List<Contact> contacts)
        {
            listView1.Items.Clear();
            foreach (Contact c in contacts)
            {
                AddListView(c);
            }
        }

        private void AddListView(Contact contact)
        {
            ListViewItem item1 = new ListViewItem(contact.Nom, 0);
            item1.SubItems.Add(contact.Prenom);
            item1.SubItems.Add(contact.DateNaissance.ToShortDateString());
            item1.SubItems.Add(contact.Email);
            listView1.Items.Add(item1);
        }


        private bool SelectContact(out Contact contact, string titre = "")
        {
            var selected = listView1.SelectedItems;
            if (selected.Count == 0)
            {
                MessageBox.Show("Il n'a pas de ligne sélectionner", titre, MessageBoxButtons.OK, MessageBoxIcon.Error);
                contact = null;
            }
            else
            {
                contact = dao.FindByEmail(selected[0].SubItems[3].Text, false);
            }
            return contact != null;
        }

        private Contact SaisirContact(string titre = "",Contact c = null )
        {
            Contact rc = null;
            Form2 f = new Form2(c);
            if (f.ShowDialog(this) == DialogResult.OK)
            {
                Contact search = dao.FindByEmail(f.ContactForm.Email);
                if ((search != null && c == null) || (search != null && search.Id != c.Id))
                {
                    MessageBox.Show("L'email existe déjà", titre, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    rc = f.ContactForm;
                }
            }
            f.Close();
            f.Dispose();
            return rc;
        }
    }
}
