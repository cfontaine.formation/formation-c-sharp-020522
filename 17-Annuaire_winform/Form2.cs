
using _15_AdoDotNet;
using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace _17_Annuaire_winform
{
    public partial class Form2 : Form
    {
        public Contact ContactForm { get; private set; }

        public Form2()
        {
            InitializeComponent();
        }
        public Form2(Contact contact) : this()
        {
            if (contact != null)
            {
                ContactForm = contact;
                Text = "Modifier un Contact";
                addModifBtn.Text = "Modifer";
                prenomTextbox.Text = contact.Prenom;
                nomTextbox.Text = contact.Nom;
                dateNaissancePicker.Value = contact.DateNaissance;
                emailTextbox.Text = contact.Email;
            }
        }

        private void addModifBtn_Click(object sender, EventArgs e)
        {
            if (!CheckContactInfo(out string msgErr))
            {
                MessageBox.Show(msgErr, "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Error);
                DialogResult = DialogResult.Cancel;
            }
            else
            {
                if (ContactForm == null)
                {
                    ContactForm = new Contact(prenomTextbox.Text, nomTextbox.Text, emailTextbox.Text, dateNaissancePicker.Value);
                }
                else
                {
                    ContactForm.Prenom = prenomTextbox.Text;
                    ContactForm.Nom = nomTextbox.Text;
                    ContactForm.DateNaissance = dateNaissancePicker.Value;
                    ContactForm.Email = emailTextbox.Text;
                }
                DialogResult = DialogResult.OK;
            }
        }

        private bool CheckContactInfo(out string messageErr)
        {
            if (prenomTextbox.Text == "")
            {
                messageErr = "Le champs prénom ne doit pa être vide";
                return false;
            }
            else if (nomTextbox.Text == "")
            {
                messageErr = "Le champs nom ne doit pa être vide";
                return false;
            }
            else if (dateNaissancePicker.Value > DateTime.Now)
            {
                messageErr = "La date de naissance n'est pas valide";
                return false;
            }
            else if (!Regex.IsMatch(emailTextbox.Text, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase))
            {
                messageErr = "La email n'est pas valide";
                return false;
            }
            messageErr = "";
            return true;
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
