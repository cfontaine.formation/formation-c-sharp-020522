﻿namespace _10_Polymorphisme
{
    internal interface IPeutVoler
    {
        void Atterir();

        void Decoller();
    }
}
