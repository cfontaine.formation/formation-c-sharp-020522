﻿using System;

namespace _10_Polymorphisme
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region Polymorphisme
            //Animal a1 = new Animal(3000, 4);  // Impossible la classe est abstraite
            //a1.EmettreSon();

            Chien ch1 = new Chien(8000, 4, "Rolo");
            ch1.EmettreSon();

            Animal a2 = new Chien(1500, 1, "Laika");    // On peut créer une instance de Chien (classe fille) qui aura une référence, une référence Animal (classe mère)
                                                        // L'objet Chien sera vu comme un Animal, on ne pourra pas accèder aux propriétés et aux méthodes propre au chien (Nom,...)
            a2.EmettreSon();                            // Comme la méthode est virtual dans Animal et est rédéfinie dans Chien, c'est la méthode de Chien qui sera appelée

            //if (a1 is Chien)           // test si a2 est de "type" Chien
            //{
            // Pour passer d'une super-classe à une sous-classe, il faut le faire explicitement avec un cast ou avec l'opérateur as
            //    Chien c2 =(Chien) a2;   
            //    Chien ch2 = a1 as Chien;       // as equivalant à un cast pour un objet
            //    Console.WriteLine(ch2.Nom);    // avec la référence ch2 (de type Chien), on a bien accès à toutes les propriétées de la classe Chien
            //}

            if (a2 is Chien ch2)    // Avec l'opérateur is, on peut tester si a2 est de type Chien et faire la conversion en même temps
            {
                // Chien ch2 = (Chien)a;
                // Chien ch2 = a2 as Chien;
                ch2.EmettreSon();
            }

            Animalerie an = new Animalerie(20);
            an.Ajouter(new Chien(8000, 4, "Rolo"));
            an.Ajouter(new Chien(2000, 2, "Laika"));
            an.Ajouter(new Chat(4000, 3, 9));
            an.Ajouter(new Chat(5000, 6, 7));
            an.Ecouter();
            #endregion

            #region object
            Animal a3 = new Chien(3000, 7, "Idefix");

            // ToString()
            // Console.WriteLine(a3.ToString());
            Console.WriteLine(a3);

            // Equals
            Animal a4 = new Chien(3000, 7, "Idefix");
            Console.WriteLine(a3 == a4); // si l'opérateur == n'est pas redéfinit, on compare l'égalité des références comme se sont 2 objets différents -> false
            Console.WriteLine(a4.Equals(a3));   // si Equal a été redéfinie -> true

            // Type =>Reflexion
            Type t = a4.GetType();
            Console.WriteLine(t.Name);
            foreach (var prop in t.GetProperties())
            {
                Console.WriteLine(prop.Name);
            }
            foreach (var meth in t.GetMethods())
            {
                Console.WriteLine(meth.Name);
            }
            #endregion

            #region Interface
            // On peut utiliser une référence vers une interface pour référencer une classe qui l'implémente
            IPeutMarcher ipm1 = new Chat(300, 5, 4);    // On peut utiliser une référence vers une interface pour référencer une classe qui l'implémente
            ipm1.Courir();

            IPeutMarcher[] tab = new IPeutMarcher[5]; // On peut utiliser une référence vers une interface pour référencer une classe qui l'implémente
            tab[0] = new Chien(4, 4000, "Idefix");
            tab[1] = new Chien(8, 5000, "Laika");
            tab[2] = new Chat(4, 2500, 6);
            tab[3] = new Canard(8, 5000);
            tab[4] = new Chat(2, 3500, 7);

            foreach (var v in tab)
            {
                v.Courir();

            }
            #endregion

            Console.ReadKey();
        }
    }
}
