﻿using System;
using System.Collections.Generic;

namespace _10_Polymorphisme
{
    internal class Chien : Animal, IPeutMarcher  // La classe Chien hérite de la classe Animal et implémente l'interface IPeutMarcher
    {
        public string Nom { get; set; }
        public Chien(int poid, int age, string nom) : base(poid, age)
        {
            Nom = nom;
        }

        public sealed override void EmettreSon()    // Redéfinition obligatoire de la méthode abstraite EmmettreUnSon de la classe Animal 
        {                                           // sealed sur une méthode interdit la redéfiniton de la méthode dans les sous-classes
            Console.WriteLine($"{Nom} aboie");
        }

        public override string ToString()
        {
            return $" {base.ToString()} Nom={Nom}";
        }

        public override bool Equals(object obj)
        {
            return obj is Chien chien &&
                   Poid == chien.Poid &&
                   Age == chien.Age &&
                   Nom == chien.Nom;
        }

        public override int GetHashCode()
        {
            int hashCode = -589909282;
            hashCode = hashCode * -1521134295 + Poid.GetHashCode();
            hashCode = hashCode * -1521134295 + Age.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Nom);
            return hashCode;
        }

        public void Marcher()   // Impléméntation de la méthode Marcher de l'interface IPeutMarcher
        {
            Console.WriteLine($" {Nom} marche");
        }

        public void Courir()    // Impléméntation de la méthode Courrir de l'interface  IPeutMarcher
        {
            Console.WriteLine($" {Nom} court");
        }
    }
}
