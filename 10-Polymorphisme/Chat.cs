﻿using System;

namespace _10_Polymorphisme
{
    internal class Chat : Animal, IPeutMarcher
    {
        public int NbVie { get; set; } = 9;

        public Chat() : base(3000, 1)
        {

        }
        public Chat(int poid, int age, int nbVie) : base(poid, age)
        {
            NbVie = nbVie;
        }

        public override void EmettreSon()
        {
            Console.WriteLine("Le chat miaule");
        }

        public void Marcher()
        {
            Console.WriteLine($" Le chat marche");
        }

        public void Courir()
        {
            Console.WriteLine($" Le Chat court");
        }
    }
}
