﻿namespace _10_Polymorphisme
{
    internal class Labrador : Chien
    {
        public Labrador(int poid, int age, string nom) : base(poid, age, nom)
        {
            // Comme la méthode est sealed dans la classe mère, on ne peut plus redéfinir la méthode
            //public override void EmettreSon()
            //{

            //}
        }

    }
}
