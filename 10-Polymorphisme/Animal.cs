﻿namespace _10_Polymorphisme
{
    internal abstract class Animal  // Animal est une classe abstraite, elle ne peut pas être instantiée elle même
    {                                // mais uniquement par l'intermédiaire de ses classses filles
        public int Poid { get; set; }

        public int Age { get; set; }

        public Animal(int poid, int age)
        {
            Poid = poid;
            Age = age;
        }

        //public virtual void EmettreSon()
        //{
        //    Console.WriteLine("L'animal emet un son");
        //}

        public abstract void EmettreSon();  // Méthode abstraite qui doit obligatoirement redéfinit par les classes filles

        // Redéfinition des Méthodes d'object
        // ToString => retourne une chaîne qui représente l'objet
        public override string ToString()
        {
            return $"Poid={Poid} Age={Age}";
        }

        // Equals => permet de tester si l'objet est égal à l'objet passer en paramètre
        // Si on redéfinie Equals, il faut aussi redéfinir GetHashCode
        public override bool Equals(object obj)
        {
            return obj is Animal animal &&
                   Poid == animal.Poid &&
                   Age == animal.Age;
        }

        // Quand on redéfinie Equals, il faut obligatoirement redéfinir GetHashCode
        public override int GetHashCode()
        {
            int hashCode = -1353125535;
            hashCode = hashCode * -1521134295 + Poid.GetHashCode();
            hashCode = hashCode * -1521134295 + Age.GetHashCode();
            return hashCode;
        }
    }
}
