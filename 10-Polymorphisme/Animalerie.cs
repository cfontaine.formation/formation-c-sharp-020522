﻿namespace _10_Polymorphisme
{
    internal class Animalerie
    {
        Animal[] _places;

        int placeOccupe;

        public Animalerie(int nbPlace)
        {
            _places = new Animal[nbPlace];
        }

        public void Ajouter(Animal a)
        {
            if (placeOccupe < _places.Length)
            {
                _places[placeOccupe] = a;
                placeOccupe++;
            }
        }

        public void Ecouter()
        {
            for (int i = 0; i < placeOccupe; i++)
            {
                _places[i].EmettreSon();
            }
        }
    }
}
