﻿using System;

namespace _10_Polymorphisme
{
    internal class Canard : Animal, IPeutMarcher, IPeutVoler     // On peut implémenter plusieurs d'interface
    {
        public Canard(int poid, int age) : base(poid, age)
        {
        }
        public void Marcher()
        {
            Console.WriteLine($" Le canard marche");
        }

        public void Courir()
        {
            Console.WriteLine($" Le canard court");
        }

        public override void EmettreSon()
        {
            Console.WriteLine($"Coin coin");
        }

        public void Atterir()
        {
            Console.WriteLine($" Le canard atterit");
        }

        public void Decoller()
        {
            Console.WriteLine($" Le canard décolle");
        }
    }
}
